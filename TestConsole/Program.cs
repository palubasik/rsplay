﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TweetSharp;
using System.Security.Cryptography;
using Un4seen.Bass;

namespace TestConsole
{
    class Program
    {
        internal static string GenerateSalt()
        {
            byte[] buf = new byte[16];
            (new RNGCryptoServiceProvider()).GetBytes(buf);
            return Convert.ToBase64String(buf);
        }

        internal static string EncodePassword(string pass, int passwordFormat, string salt)
        {
            if (passwordFormat == 0) // MembershipPasswordFormat.Clear
                return pass;

            byte[] bIn = Encoding.Unicode.GetBytes(pass);
            byte[] bSalt = Convert.FromBase64String(salt);
            byte[] bAll = new byte[bSalt.Length + bIn.Length];
            byte[] bRet = null;

            Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
            Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);
            if (passwordFormat == 1)
            { // MembershipPasswordFormat.Hashed
                HashAlgorithm s = HashAlgorithm.Create("SHA1");
                // Hardcoded "SHA1" instead of Membership.HashAlgorithmType
                bRet = s.ComputeHash(bAll);
            }
            else
            {
               // bRet = EncryptPassword(bAll);
            }
            return Convert.ToBase64String(bRet);
        }


        public static string EncodePassword(string pass, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
            byte[] inArray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inArray);
        } 

        static void Main(string[] args)
        {
            BassNet.Registration("buddyknox@usa.org", "2X11841782815");

            BASS_DEVICEINFO info = new BASS_DEVICEINFO();
            for (int n = 0; Bass.BASS_GetDeviceInfo(n, info); n++)
            {
                Console.WriteLine(info.ToString());
            }


            TwitterService ts = new TwitterService();
            TwitterSearchResult tsr = ts.Search("%23radiostart");
            foreach(var status in tsr.Statuses)
            Console.WriteLine(status.Text);
        }
    }
}
