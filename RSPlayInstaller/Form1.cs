﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RSPlayInstaller
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        const string UPDATE_FILE_PATH = "http://dl.dropbox.com/u/18364084/RSPlay.zip";
        string localPath = AppDomain.CurrentDomain.BaseDirectory;
        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Укажите, пожалуйста, директорию для установки приложения";
            if (fbd.ShowDialog() != DialogResult.OK)
                return;
            textBox1.Text = fbd.SelectedPath;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = AppDomain.CurrentDomain.BaseDirectory;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            noError = true;
            button1.Enabled = false;
            localPath = textBox1.Text;
            backgroundWorker1.RunWorkerAsync();
        }
        ManualResetEvent ma = new ManualResetEvent(false);

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string zip_file = Path.Combine(localPath, "RSPlay.zip");
                WebClient client = new WebClient();
                client.DownloadProgressChanged += client_DownloadProgressChanged;
                ma.Reset();
                client.DownloadFileAsync(new Uri(UPDATE_FILE_PATH), zip_file);
                client.DownloadFileCompleted += client_DownloadFileCompleted;
                ma.WaitOne();
                ZipUtil.UnZipFiles(zip_file,localPath, string.Empty, true);
                backgroundWorker1.ReportProgress(95);
            }
            catch (Exception ex)
            {
                noError = false;
                MessageBox.Show(string.Format("Ошибка: {0}", ex.Message), "Error :(", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally 
            {
                backgroundWorker1.ReportProgress(100);
            }
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            ma.Set();
        }

      

        bool noError = true;
        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
                progressBar1.Value = e.ProgressPercentage < 90 ? e.ProgressPercentage : 90;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            if (progressBar1.Value == 100)
            {
                if (noError)
                {
                    System.Diagnostics.Process.Start(Path.Combine(localPath, "RSPlay.exe"));
                    this.Close();
                }
                else
                    button1.Enabled = true;
            }
        }
    }
}
