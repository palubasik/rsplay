﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Un4seen.Bass;
using RSPlay.Engine;
using System.Threading;
using Common;

namespace RSPlay
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           

            bool mutexCreated = false;
            System.Threading.Mutex mutex = new System.Threading.Mutex( true, Constants.APP_NAME + Constants.APP_NAME, out mutexCreated );

            if( !mutexCreated )
              {
                  MessageBox.Show(
                    "Приложение уже запущено",
                    Constants.APP_NAME,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                  mutex.Close();
                  return;
   
              }
            try
            {
                BassNet.Registration("buddyknox@usa.org", "2X11841782815");
            }
            catch { }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(UnhandledThreadExceptionHandler);
            Application.Run(new Form1());
            mutex.Close();
        }

        public static void UnhandledThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            HandleUnhandledException(e.Exception);
        }

        public static void HandleUnhandledException(Exception e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(LoggerWorker), e);

            ErrorForm er = new ErrorForm();
            er.ErrorInformation = String.Format("Message: {0}\n\nStack: {1}\n\nInnerException:{2}", e.Message, e.StackTrace, e.InnerException != null ? e.InnerException.Message : "Empty");
            if (er.ShowDialog() == System.Windows.Forms.DialogResult.No)
            {
                Application.Exit();
            }
        }

        static void LoggerWorker(object data)
        {
            ErrorLogger.Instance.WriteError(data as Exception);
        }

    }
}
