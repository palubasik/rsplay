﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using System.Xml;
using System.Threading;
using System.Web;
using System.IO;
using System.Net;
using RSPlay.Properties;
using RSPlay.UserControls;
using Common;
using Un4seen.Bass;
using TweetSharp;
using System.Media;
using SKYPE4COMLib;

namespace RSPlay
{
    public partial class Form1 : Form
    {
        GlobalEngine engine;

        delegate void UpdateAfisha(List<RadioStartProgramData> data);
        delegate void UpdateNews(List<RadioStartNewsData> data);
        delegate void UpdateChat(List<string> users, List<ChatMessageData> messages);
        delegate void UpdateMsg(object message);

        public Form1()
        {
            InitializeComponent();
            this.Text = string.Format("{0} {1}", Constants.APP_NAME, Constants.Version);
        }

        void engine_OnBeginUpdate()
        {
            try
            {
                Helper.UIThread(this, () =>
                {
                    string message = "Получение новостей...";
                    UpdateStatus(message);
                });
            }
            catch 
            {
                //CHECK THIS IN FUTURE
            }
        }

        void UpdateStatus(object message)
        {
            if (message.ToString() == "Готово") 
                toolStripStatusLabel.Image = Resources.done;
            else
                toolStripStatusLabel.Image = Resources.processing;

            toolStripStatusLabel.Text = message.ToString();
        }

        void engine_OnProgramUpdate(List<RadioStartProgramData> list)
        {
            try
            {
                Helper.UIThread(this, () => { UpdateRichTextBoxAfisha(list); });
            }
            catch 
            {
                //CHECK THIS IN FUTURE
            }
        }

        void UpdateRichTextBoxAfisha(List<RadioStartProgramData> list) 
        {
            panelPrograms.Controls.Clear();
            foreach (RadioStartProgramData pdata in list)
            {
                ProgramDisplayUserControl programDisplay = new ProgramDisplayUserControl(pdata);
                panelPrograms.Controls.Add(programDisplay);
                programDisplay.Dock = DockStyle.Top;
                programDisplay.BringToFront();
                if (pdata.IsActive) 
                {
                    pictureBoxWhatOnline.LoadAsync(pdata.PictureURL);
                    richTextBoxWhatOnlineDesc.Text = pdata.Description2;
                    labelWhatOnlineCaption.Text = pdata.Title;
                }
            }
        }

        void engine_OnNewsUpdate(List<RadioStartNewsData> list)
        {
            try
            {
                Helper.UIThread(this, () =>
                {
                    UpdateRichTextBoxNews(list);
                });
            }
            catch 
            {
                //CHECK THIS IN FUTURE
            }
        }

        class ChatUserData
        {
            public Color Color = Color.Black;
            public string Name;
        }

        static Random rnd = new Random();

        Color GetRandomColor()
        {
            Color res;
            int rand = rnd.Next(18);
            if (users.Count < 18)
            {
                for (int i = rand; i < 18; i++)
                {
                    res = GetColorFromColorTable(rand);
                    if (users.Find(x => x.Color == res) == null)
                        return res;
                }
                for (int i = 0; i < rand; i++)
                {
                    res = GetColorFromColorTable(rand);
                    if (users.Find(x => x.Color == res) == null)
                        return res;
                }
            }
            return GetColorFromColorTable(rand);
        }

        private  Color GetColorFromColorTable(int i)
        {
            switch (i)
            {
                case 0:
                    return Color.Black;
                case 1:
                    return Color.DarkOliveGreen;
                case 2:
                    return Color.DarkMagenta;
                case 3:
                    return Color.Navy;
                case 4:
                    return Color.DarkGreen;
                case 5:
                    return Color.DeepPink;
                case 6:
                    return Color.Firebrick;
                case 7:
                    return Color.Goldenrod;
                case 8:
                    return Color.DarkViolet;
                case 9:
                    return Color.DodgerBlue;
                case 10:
                    return Color.Indigo;
                case 11:
                    return Color.LawnGreen;
                case 12:
                    return Color.OrangeRed;
                case 13:
                    return Color.Lime;
                case 14:
                    return Color.Aqua;
                case 15:
                    return Color.Orange;
                case 16:
                    return Color.Red;
                case 17:
                    return Color.Sienna;
                default:
                    return Color.Black;
            }
        }
 
        void UpdateRichTextBoxNews(List<RadioStartNewsData> list) 
        {
            panelNews.Controls.Clear();
            foreach (RadioStartNewsData ndata in list)
            {
                NewsDisplayUserControl programDisplay = new NewsDisplayUserControl(ndata);
                panelNews.Controls.Add(programDisplay);
                programDisplay.Dock = DockStyle.Top;
                programDisplay.BringToFront();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
      
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (SettingInfo.Instance.WasNotExist)
            {
                if (MessageBox.Show("Создать ярлык на рабочем столе?", "Первичный запуск программы", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                {
                    appShortcutToDesktop("RadioStart Player");
                }
                SendFirstRunEmail();
            }
            //if (SettingInfo.Instance.SettingData.CheckUpdate) 
            //{
            //    if(engine.CheckForUpdate())
            //        SettingInfo.Instance.SettingData.CheckUpdate = false;
            //}
            engine = new GlobalEngine();
            
            buttonTwiLike.Enabled = false;
            buttonLastFMLike.Enabled = false;
            trackBar1.Value = engine.GetVolume();
            buttonSendMessage.KeyDown += new KeyEventHandler(Form1_KeyDown);
            engine.OnNewsUpdate += new UpdateBlogNews(engine_OnNewsUpdate);
            engine.OnProgramUpdate += new UpdateProgramNews(engine_OnProgramUpdate);
            engine.OnBeginUpdate += new ChangeEvent(engine_OnBeginUpdate);
            engine.OnChangeTitle += new ChangeTitle(engine_OnChangeTitle);
            engine.OnEndUpdate += new ChangeEvent(engine_OnEndUpdate);
            engine.OnPlayerCheckState += new PlayerChannelStateEvent(engine_OnPlayerCheckState);
            engine.OnUpdateNewVersion += new ChangeEvent(engine_OnUpdateNewVersion);
            engine.OnChatUpdate += new ChatEvent(engine_OnChatUpdate);
            engine.OnTweetsTake += new UpdateTweets(engine_OnTweetsTake);
            аккаунтВтвиттереToolStripMenuItem.Checked = engine.isAuthorizedInTwitter();
            проверятьНаОбновленияToolStripMenuItem.Checked = SettingInfo.Instance.SettingData.CheckUpdate;
            запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.Checked = SettingInfo.Instance.SettingData.AutoStartPlayer;
            свернутьВТрейВместоЗакрытияToolStripMenuItem.Checked = SettingInfo.Instance.SettingData.HideInsteadClose;

            textBoxChatSendMessage.MouseWheel += new MouseEventHandler(textBoxChatSendMessage_MouseWheel);
            UpdateStatus("Готово");
            if(SettingInfo.Instance.SettingData.AutoStartPlayer)
                buttonPlay_Click(null, null);
            if (SettingInfo.Instance.SettingData.CheckUpdate)
                engine.StartAutomaticUpdate();
            trackBar1.Value = SettingInfo.Instance.SettingData.Volume == 0 ? 70 : SettingInfo.Instance.SettingData.Volume;
            trackBar1_Scroll(null, null);
            //check this
            lastfmToolStripMenuItem.Checked = engine.isAuthorizedLastfm();
            if (lastfmToolStripMenuItem.Checked)
            {
                CreateLastfmSubMenu();
            }
            
            engine.StartTwitterUpdate();
            engine.StartNewsUpdater();
            try
            {
                LastChatMessage = ReadLastLine(Constants.CHAT_HISTORY_PATH);
            }
            catch { }

            SettingInfo.Instance.SettingData.ChatSound = !SettingInfo.Instance.SettingData.ChatSound;
            pictureBox1_Click(null, null);
        }

        void textBoxChatSendMessage_MouseWheel(object sender, MouseEventArgs e)
        {
            if (chatCtrlPressed)
            {
                if (e.Delta > 0)
                    richTextBoxChatMessages.ZoomFactor += 0.1f;
                else
                    richTextBoxChatMessages.ZoomFactor -= 0.1f;
            }
            else
            {
                if (e.Delta > 0)
                    richTextBoxChatMessages.ScrollLineUp();
                else
                    richTextBoxChatMessages.ScrollLineDown();
            }
        }

        private void SendFirstRunEmail()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback((x) =>
            {
                try
                {
                    MailSender.Instance.Subject = string.Format("First run of {0} v{1} on {2}", Constants.APP_NAME, Constants.Version, SettingInfo.Instance.SettingData.MachineIdentifier);
                    MailSender.Instance.Message = String.Format("{0}{1}{2}{1}", SettingInfo.Instance.SettingData.MachineIdentifier, Environment.NewLine, Environment.OSVersion.VersionString);
                    MailSender.Instance.SendMailLocal();
                }
                catch { }
            }), string.Empty);
        }

        public static String ReadLastLine(String path)
        {
            String retval = "";
            FileStream fs = new FileStream(path, FileMode.Open);
            for (long pos = fs.Length - 2; pos > 0; --pos)
            {
                fs.Seek(pos, SeekOrigin.Begin);
                // Try to detect the linefeed in the file
                StreamReader ts = new StreamReader(fs);
                retval = ts.ReadToEnd();
                int eol = retval.IndexOf("\n");
                if (eol >= 0)
                {
                    string str = retval.Substring(eol + 1);
                    if (string.IsNullOrEmpty(str))
                        continue;
                    // Found it
                    fs.Close();

                  
                    return str;
                }
                //ts.Close();    Don't do this!!
                //ts.Dispose();  Or that!!
            }
            fs.Close();
            return retval;
        }

//        long last_twi_id = -1;
        string LastChatMessage = string.Empty;

        void engine_OnTweetsTake(TweetSharp.TwitterSearchResult results)
        {
            //Helper.UIThread(this, () =>
            //{
            //    List<TwitterSearchStatus> lst = new List<TwitterSearchStatus>(results.Statuses);
            //    if (lst.Count == 0 || lst[lst.Count - 1].Id == last_twi_id)
            //        return;
            //    last_twi_id = lst[lst.Count - 1].Id;
            //    richTextBoxTwitter.Clear();
            //    foreach (TwitterSearchStatus status in results.Statuses)
            //    {

            //        //richTextBoxChatMessages.InsertImage(status.Author.ProfileImageUrl
            //        richTextBoxTwitter.SelectionColor = Color.Black;
            //        richTextBoxTwitter.AppendText(String.Format("[{0}] ", status.CreatedDate));
            //        richTextBoxTwitter.SelectionColor = Color.BlueViolet;
            //        richTextBoxTwitter.AppendText(String.Format("{0} ", status.Author.ScreenName));
            //        richTextBoxTwitter.SelectionColor = Color.Black;
            //        richTextBoxTwitter.AppendText(String.Format("{0} ", status.Source));
            //        richTextBoxTwitter.AppendText(Environment.NewLine);
            //    }
            //});
        }

        void engine_OnUpdateNewVersion()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback((x) =>
                {
                    Helper.UIThread(this, () =>
                    {
                        toolStripMenuItemExit_Click(null, null);
                    });
                }
            ));
        }

        void engine_OnChatUpdate(List<string> users, List<ChatMessageData> messages)
        {
            try
            {
                Helper.UIThread(this, () => { ChatUpdate(users, messages); });
            }
            catch 
            {
                //CHECK THIS IN FUTURE
            }
        }

        void ChatUpdate(List<string> users, List<ChatMessageData> messages)
        {
            richTextBoxChatUsers.Clear();
            List<ChatUserData> colorized_users = ColorizeUsers(users);
            foreach (ChatUserData user in colorized_users)
                FormatChatUser(user);
            foreach (ChatMessageData message in messages)
                FormatRichChatMessage(message);
            if (messages.Count > 0 && SettingInfo.Instance.SettingData.ChatSound)
            {
                PlaySoundIncomeMessages();
            }
            richTextBoxChatMessages.ScrollToEnd();
            SaveChatHistory(messages);
        }

        private List<ChatUserData> ColorizeUsers(List<string> users_text)
        {
            List<ChatUserData> old_users = new List<ChatUserData>();
            old_users.AddRange(users);
            users.Clear();
            foreach (string user_text in users_text)
            {
                ChatUserData u = old_users.Find(x => x.Name == user_text);
                if (u == null)
                    users.Add(new ChatUserData() { Color = GetRandomColor(), Name = user_text });
                else
                    users.Add(u);
            }
            return users;
        }

        private static void PlaySoundIncomeMessages()
        {
            try
            {
                SoundPlayer sp = new SoundPlayer(Resources.ding);
                sp.Play();
            }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
            }
        }

        private void SaveChatHistory(List<ChatMessageData> messages)
        {
            if (messages.Count == 0)
                return;

            ChatMessageData last_message = messages[messages.Count - 1];
            string msg = FormatTextChatMsg(last_message);

            if (msg != LastChatMessage)
            {
                foreach (ChatMessageData message in messages)
                {
                    string msg_to_save = FormatTextChatMsg(message);
                    if (string.IsNullOrEmpty(msg_to_save) )
                        continue;
                    try
                    {
                        File.AppendAllText(Constants.CHAT_HISTORY_PATH, msg_to_save, Encoding.UTF8);
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.Instance.WriteError(e);
                    }
                    LastChatMessage = msg_to_save;
                }
            }
        }
        private string FormatTextChatMsg(ChatMessageData message) 
        {
            return String.Format("[{0}] {1}: {2} {3}", message.DateTimeStr, message.User, message.Message,Environment.NewLine);
        }
        private void FormatChatUser(ChatUserData user)
        {
            richTextBoxChatUsers.SelectionColor = user.Color;
            richTextBoxChatUsers.AppendText(user.Name);
            richTextBoxChatUsers.AppendText(Environment.NewLine);
            richTextBoxChatUsers.SelectionColor = Color.Black;
        }

        private void FormatRichChatMessage(ChatMessageData message)
        {
            //richTextBoxChatMessages.SelectionColor = message.UserColor != Color.LightGray ? Color.Black : message.UserColor;
            //richTextBoxChatMessages.AppendText(String.Format("[{0}] ", message.DateTimeStr));
            //richTextBoxChatMessages.SelectionColor = message.UserColor;
            //richTextBoxChatMessages.AppendText(String.Format("{0}", message.User));
            //richTextBoxChatMessages.SelectionColor = message.UserColor != Color.LightGray ? Color.Black : message.UserColor;

            ChatUserData user = GetColorizedUser(message.User);
            richTextBoxChatMessages.SelectionColor = user.Color != Color.DimGray ? Color.Black : user.Color;
            richTextBoxChatMessages.AppendText(String.Format("[{0}] ", message.DateTimeStr));
            if (message.IsPrivate)
            {
                richTextBoxChatMessages.SelectionColor = Color.Red;
                richTextBoxChatMessages.AppendText(message.MyMessage ? "лично для " : "лично от ");
            }
            richTextBoxChatMessages.SelectionColor = user.Color;
            
            richTextBoxChatMessages.AppendText(String.Format("{0}",message.User));
            richTextBoxChatMessages.SelectionColor = user.Color != Color.DimGray ? Color.Black : user.Color;
            //string nick = engine.ChatGetNick();
            //int idx = message.Message.IndexOf(nick);
            //if (message.Message.StartsWith(nick) || 
            //    (idx > 0 && !char.IsLetterOrDigit(message.Message[idx - 1]) && !char.IsLetterOrDigit(message.Message[idx +nick.Length])))
            //{ 
            //    Font f = richTextBoxChatMessages.SelectionFont;
            //    Font bolded = new Font(f, FontStyle.Bold);
            //    richTextBoxChatMessages.AppendText(String.Format(": {0}", message.Message.Substring(0,idx)));
            //    richTextBoxChatMessages.SelectionFont = bolded;
            //    richTextBoxChatMessages.AppendText(nick);
            //    richTextBoxChatMessages.SelectionFont = f;
            //    richTextBoxChatMessages.AppendText(message.Message.Substring(idx + nick.Length));
            //}
            //else
            //{
            string[] message_spl = null;
            List<SmileData> smile_data = Smiles.GetSmilesOnMessage(message.Message, out message_spl);
            for (int i = 0, len = message_spl.Length; i < len; i++)
            {
                if (i == 0)
                    richTextBoxChatMessages.AppendText(String.Format(": {0}", message_spl[i]));
                else
                    richTextBoxChatMessages.AppendText(message_spl[i]);
                if (smile_data.Count > i)
                    richTextBoxChatMessages.InsertImage(smile_data[i].Smile);
            }
            
            //}
            richTextBoxChatMessages.AppendText(Environment.NewLine);
        }

        List<ChatUserData> users = new List<ChatUserData>();

        private ChatUserData GetColorizedUser(string p)
        {
            ChatUserData user = users.Find(x => x.Name == p);

            return user == null ? new ChatUserData() { Color = Color.DimGray } : user;
        }

        void engine_OnPlayerCheckState(bool isActive)
        {
            try
            {
                Helper.UIThread(this, () =>
                {
                    buttonPlay.Enabled = !isActive;
                    buttonStop.Enabled = !buttonPlay.Enabled;

                    //if (!isActive)
                    //{
                    //    foreach (ToolStripMenuItem item in эффектыToolStripMenuItem.DropDownItems)
                    //        item.Checked = false;
                    //    buttonLastFMLike.Enabled = buttonTwiLike.Enabled = false;
                    //}
                });

            }
            catch(Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
            }
        }

        void engine_OnEndUpdate()
        {
            Helper.UIThread(this, () =>
               {
                   UpdateStatus("Готово");
               });
        }

        string current_title = string.Empty;

        void engine_OnChangeTitle(string title)
        {
            if (current_title != title)
            {
                Helper.UIThread(this, () =>
                {
                    current_title = title;
                    labelPlayed.Text = current_title; buttonTwiLike.Enabled = true;
                    buttonLastFMLike.Enabled = true;
                    if (this.Visible == false)
                    {
                        notifyIcon.BalloonTipText = title;
                        notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                        notifyIcon.BalloonTipTitle = Constants.APP_NAME;
                        notifyIcon.ShowBalloonTip(5000);
                    }
                });
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
            //buttonPlay.Enabled = true;
            buttonStop.Enabled = false;
            buttonTwiLike.Enabled = false;
            buttonLastFMLike.Enabled = false;
            //foreach (ToolStripMenuItem item in эффектыToolStripMenuItem.DropDownItems)
            //    item.Checked = false;
            UpdateStatus("Остановка...");
            engine.StopPlayer();
            pictureBoxWhatOnline.Image = Resources.logo;
            labelPlayed.Text = string.Empty;
            labelWhatOnlineCaption.Text = string.Empty;
            richTextBoxWhatOnlineDesc.Text = string.Empty;
            current_title = string.Empty;
            UpdateStatus("Готово");
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            buttonPlay.Enabled = false;
            buttonStop.Enabled = true;
            UpdateStatus("Начало проигрывания...");
            engine.StartPlayer();
            engine.SetVolume(trackBar1.Value);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.OpenLink(Constants.RadioStartSite.SERVER_URL);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                notifyIcon.Visible = true;
                Hide();
            }
        }
        
        private void appShortcutToDesktop(string linkName)
        {
            string deskFile = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\" + linkName + ".url";

            using (StreamWriter writer = new StreamWriter(deskFile))
            {
                string app = System.Reflection.Assembly.GetExecutingAssembly().Location;
                writer.WriteLine("[InternetShortcut]");
                writer.WriteLine("URL=file:///" + app);
                writer.WriteLine("IconIndex=0");
                string icon = app.Replace('\\', '/');
                writer.WriteLine("IconFile=" + icon);
                writer.Flush();
                SettingInfo.Instance.SettingData.DeskFile = deskFile;
            }
        }

        bool exit = false;
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingInfo.Instance.SettingData.Volume = trackBar1.Value;
            if (SettingInfo.Instance.SettingData.HideInsteadClose && e.CloseReason == CloseReason.UserClosing && !exit)
            {
                notifyIcon.Visible = true;
                Hide();
                e.Cancel = true;
                return;
            }
            this.Hide();
            engine.Dispose();

        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            if (this.Visible == false)
            {
                notifyIcon.Visible = false;
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            exit = true;
            this.Close();
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            exit = true;
            this.Close();
        }

        private void toolStripMenuItemShow_Click(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex) 
            {
                case 0:
                    break;
                case 1:
                    if (string.IsNullOrEmpty(engine.ChatNick)) 
                    {
                        engine.ChatLogin();
                        textBoxChatSendMessage.Focus();
                    }
                    break;
                case 2:
                    ActiveControl = panelPrograms;
                    break;
                case 3:
                    ActiveControl = panelNews;
                    //panelNews.VerticalScroll.Value = panelNews.VerticalScroll.Maximum;
                    //panelNews.Update();
                    break;
                case 4:
                    //if (!engine.isAuthorizedInTwitter()) 
                    //{
                    //    if(MessageBox.Show("Требуется авторизация в твиттере. Авторизоваться?", "Интеграция с твиттером", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    //        engine.TwitterAuthAction();
                    //}
                    break;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void buttonLike_Click(object sender, EventArgs e)
        {
            if (engine.SendTwiLike())
            {
                buttonTwiLike.Enabled = false;
                аккаунтВтвиттереToolStripMenuItem.Checked = true;
            }
            else
            {
                MessageBox.Show("Не удалось лайкнуть песню. Проверьте настройки", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void аккаунтВтвиттереToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (engine.isAuthorizedInTwitter()) 
            {
                if (MessageBox.Show("Вы уже авторизованы в твиттере. Хотите сменить аккаунт?", "Авторизация в твиттере", MessageBoxButtons.YesNo, MessageBoxIcon.Information) != System.Windows.Forms.DialogResult.Yes)
                {
                    аккаунтВтвиттереToolStripMenuItem.Checked = engine.isAuthorizedInTwitter();
                    return; }
            }
            if (engine.TwitterAuthAction())
            {
                SettingInfo.Instance.Save();
            }
            аккаунтВтвиттереToolStripMenuItem.Checked = engine.isAuthorizedInTwitter();
        }

        private void buttonSendMessage_Click(object sender, EventArgs e)
        {
            if (textBoxChatSendMessage.Text.StartsWith("/"))
            { 
                int idx = textBoxChatSendMessage.Text.IndexOf(" ");
                string command = textBoxChatSendMessage.Text.Substring(1);
                string data = string.Empty;
                if(idx != -1)
                {
                    command = textBoxChatSendMessage.Text.Substring(1,idx -1); 
                    data = textBoxChatSendMessage.Text.Substring(idx +1);
                }
                textBoxChatSendMessage.Text = string.Empty;
                if (!engine.ChatSendCommand(command, data))
                    MessageBox.Show("Комманда не поддерживается", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkBoxPrivat.Enabled && checkBoxPrivat.Checked)
            {
                string[] message_data = textBoxChatSendMessage.Text.Split(':');

                if (engine.ChatSendPrivateMessage(message_data[0], textBoxChatSendMessage.Text.Replace(message_data[0] + ":","")))
                    textBoxChatSendMessage.Text = string.Empty;
            }
            else
            {
                if (engine.ChatSendMessage(textBoxChatSendMessage.Text))
                    textBoxChatSendMessage.Text = string.Empty;
            }
            richTextBoxChatMessages.ScrollToEnd();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && tabControl1.SelectedIndex == 1)
                buttonSendMessage_Click(null, null);
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {

        }

        private void создатьИконкуНаРабочемСтолеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            appShortcutToDesktop("RadioStart Player");
            menuStrip1.Refresh();
        }

        private void сообщитьОБагеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SugestForm sf = new SugestForm();
            sf.ShowDialog();
        }

        private void textBoxChatSendMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode & Keys.ShiftKey) != 0)
                chatCtrlPressed = true;
            Form1_KeyDown(sender, e);
        }
        bool chatCtrlPressed = false;
        private void оПрограммеToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AboutBox af = new AboutBox();
            af.ShowDialog();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingInfo.Instance.SettingData.CheckUpdate = true;
            if (!engine.CheckForUpdate())
                MessageBox.Show("Текущая версия программы самая свежая", "Обновление", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingInfo.Instance.SettingData.AutoStartPlayer =
           запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.Checked;
            SettingInfo.Instance.Save();
        }

        private void проверятьНаОбновленияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingInfo.Instance.SettingData.CheckUpdate = проверятьНаОбновленияToolStripMenuItem.Checked;
            SettingInfo.Instance.Save();
        }

        private void выходToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            exit = true;
            this.Close();
        }

        private void свернутьВТрейВместоЗакрытияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingInfo.Instance.SettingData.HideInsteadClose = свернутьВТрейВместоЗакрытияToolStripMenuItem.Checked;
            SettingInfo.Instance.Save();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

            engine.SetVolume(trackBar1.Value);
        }

        private void buttonTwitterSend_Click(object sender, EventArgs e)
        {
            //if (engine.SendTwiMessage(textBoxTwitterSend.Text, true))
            //    textBoxTwitterSend.Text = string.Empty;
        }

        private void textBoxTwitterSend_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                buttonTwitterSend_Click(sender, e);
            }
        }

        private void buttonTwitterSend_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonTwitterSend_Click(sender, e);
            }
        }

        private void историяЧатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChatHistoryForm ch = new ChatHistoryForm();
            ch.ShowDialog();
        }

        private void проксиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProxyForm pf = new ProxyForm();
            pf.Show();
        }

        private void ProcessEffect(object sender, BASSFXType effect) 
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            if (item.Checked)
                engine.SetEffect(effect);
            else
                engine.RemoveEffect(effect);
        }
        private void фланджерToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessEffect(sender, BASSFXType.BASS_FX_DX8_FLANGER);
        }

        private void ревербToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessEffect(sender, BASSFXType.BASS_FX_BFX_REVERB);
        }

        private void эхоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessEffect(sender, BASSFXType.BASS_FX_BFX_ECHO);
        }

        private void дисторшинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessEffect(sender, BASSFXType.BASS_FX_BFX_DISTORTION);
        }

        private void хорусToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessEffect(sender, BASSFXType.BASS_FX_BFX_CHORUS);
        }

        private void вахToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessEffect(sender, BASSFXType.BASS_FX_BFX_AUTOWAH);
        }

        private void richTextBoxChatMessages_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Helper.OpenLink(e.LinkText);
        }

        private void чтоНовогоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WhatIsNewForm wf = new WhatIsNewForm();
            wf.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (engine.SendLoveLastFM())
            {
                buttonLastFMLike.Enabled = false;
                lastfmToolStripMenuItem.Checked = true;
                CreateLastfmSubMenu();
            }
        }

        private void lastfmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            настройкаToolStripMenuItem.DropDown.Hide();
            if (engine.isAuthorizedLastfm())
            {
                if (MessageBox.Show("Вы уже авторизованы в last.fm. Хотите сменить аккаунт?", "Авторизация в last.fm", MessageBoxButtons.YesNo, MessageBoxIcon.Information) != System.Windows.Forms.DialogResult.Yes)
                {
                    lastfmToolStripMenuItem.Checked = true;
                    return;
                }
            }

            if (engine.LastfmAuthAction())
            {
                SettingInfo.Instance.SettingData.EnableScrobble = true;
                SettingInfo.Instance.Save();
            }
            
            lastfmToolStripMenuItem.Checked = engine.isAuthorizedLastfm();
            if (lastfmToolStripMenuItem.Checked)
            {

                CreateLastfmSubMenu();
            }
            //автоматическиСкроблитьToolStripMenuItem.Checked = SettingInfo.Instance.SettingData.EnableScrobble;

        }

        private void CreateLastfmSubMenu()
        {
            lastfmToolStripMenuItem.DropDownItems.Clear();
            ToolStripMenuItem scrobble = new ToolStripMenuItem();
            scrobble.CheckOnClick = true;
            scrobble.Click += new EventHandler(автоматическиСкроблитьToolStripMenuItem_Click);
            scrobble.Checked = SettingInfo.Instance.SettingData.EnableScrobble;
            scrobble.Text = "Автоматически скробблить";
            lastfmToolStripMenuItem.DropDownItems.Add(scrobble);
            ToolStripMenuItem setinterval = new ToolStripMenuItem();
            setinterval.Image = Resources.setupicon;
            setinterval.Click += (o, x) =>
            {
                ScrobbleParameterForm sf = new ScrobbleParameterForm();
                sf.ScrobbleValue = SettingInfo.Instance.SettingData.ScrobbleValue;
                if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    SettingInfo.Instance.SettingData.ScrobbleValue = sf.ScrobbleValue;
                    SettingInfo.Instance.Save();
                }
            };
            setinterval.Text = "Интервал скробблинга";
            lastfmToolStripMenuItem.DropDownItems.Add(setinterval);
            ToolStripMenuItem viewlastfm = new ToolStripMenuItem();
            viewlastfm.Image = Resources.website;
            viewlastfm.Click += (o, x) => {
                Helper.OpenLink("http://lastfm.ru/home");
            };
            viewlastfm.Text = "На сайт last.fm";
            lastfmToolStripMenuItem.DropDownItems.Add(viewlastfm);
        }

        private void автоматическиСкроблитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            SettingInfo.Instance.SettingData.EnableScrobble = item.Checked;
            SettingInfo.Instance.Save();

            if (item.Checked)
                engine.StartScrobble();
            else
                engine.StopScrobble();
        }

        private void textBoxChatSendMessage_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void textBoxChatSendMessage_KeyUp(object sender, KeyEventArgs e)
        {
                chatCtrlPressed = false;
        }

        private void richTextBoxChatMessages_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void richTextBoxChatMessages_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            List<char> excludes = new List<char>();
            excludes.Add(':');
            string text = richTextBoxChatMessages.GetWordOver(e.X, e.Y);
            if (string.IsNullOrEmpty(text) || !richTextBoxChatUsers.Text.Contains(text))
                return;
            foreach (string line in richTextBoxChatUsers.Lines) 
            {
                if (line.Contains(text))
                {
                    text = line;
                    break;
                }
            }
            textBoxChatSendMessage.Text += text + ": ";
            textBoxChatSendMessage_TextChanged(null, null);
            textBoxChatSendMessage.Focus();
            textBoxChatSendMessage.SelectionStart = textBoxChatSendMessage.Text.Length;
        }

        private void richTextBoxChatUsers_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string text = richTextBoxChatUsers.GetWordOver(e.X, e.Y);
            if (string.IsNullOrEmpty(text))
                return;
            //foreach (string line in richTextBoxChatUsers.Lines)
            //{
            //    if (line.Contains(text))
            //    {
            //        text = line;
            //        break;
            //    }
            //}
            textBoxChatSendMessage.Text += text + ": ";
            textBoxChatSendMessage_TextChanged(null, null);
            textBoxChatSendMessage.Focus();
            textBoxChatSendMessage.SelectionStart = textBoxChatSendMessage.Text.Length;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CopyClipboard(richTextBoxChatMessages);
        }

        private void CopyClipboard(RichTextBoxEx source)
        {
            string text = source.GetSelectionText();
            if (String.IsNullOrEmpty(text))
                return;
            Clipboard.SetText(text);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            string text = richTextBoxChatMessages.GetSelectionText();
            textBoxChatSendMessage.Text += text;
            textBoxChatSendMessage.Focus();
            textBoxChatSendMessage_TextChanged(null, null);
            textBoxChatSendMessage.SelectionStart = textBoxChatSendMessage.Text.Length;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            FindGoogle(richTextBoxChatMessages);
        }

        private void FindGoogle(RichTextBoxEx source)
        {
            string text = source.GetSelectionText();
            if (String.IsNullOrEmpty(text))
                return;
            Helper.OpenLink(string.Format("http://google.com/#q={0}", text));
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            PlaySoundIncomeMessages();
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            PlaySoundIncomeMessages();
        }

        private void buttonLastFMLike_Click(object sender, EventArgs e)
        {
            if (engine.SendLoveLastFM())
            {
                buttonLastFMLike.Enabled = false;
                lastfmToolStripMenuItem.Checked = true;
                CreateLastfmSubMenu();
            }
            else
            {
                MessageBox.Show("Не удалось лайкнуть песню. Проверьте настройки", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            SettingInfo.Instance.SettingData.ChatSound = !SettingInfo.Instance.SettingData.ChatSound;
            if (SettingInfo.Instance.SettingData.ChatSound)
            {
                pictureBox1.Image = Resources.sound;
                this.toolTip1.SetToolTip(pictureBox1, "Звук включен");
            }
            else
            {
                pictureBox1.Image = Resources.sound_off;
                this.toolTip1.SetToolTip(pictureBox1, "Звук выключен");
            }
        }

        private void textBoxChatSendMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                e.Handled = true;//this line will do the trick
            }

        }

        private void textBoxChatSendMessage_TextChanged(object sender, EventArgs e)
        {
            string user = textBoxChatSendMessage.Text.Split(':')[0];
            if (users.Find(x => x.Name == user) != null)
                checkBoxPrivat.Enabled = true;
            else
                checkBoxPrivat.Enabled = false;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CopyClipboard(labelPlayed);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            FindGoogle(labelPlayed);
        }

        private Skype skype_machine;
        private void button2_Click_3(object sender, EventArgs e)
        {
            try
            {
                skype_machine = new Skype();
                if (!skype_machine.Client.IsRunning)
                {
                    skype_machine.Client.Start(true, true);
                }
                skype_machine.Attach(6, true);
                skype_machine.Client.Focus();
                bool found = false;
                foreach (User u in skype_machine.Friends)
                {
                    if (u.Handle == "startradio")
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    skype_machine.Client.OpenAddContactDialog("startradio");
                else
                    skype_machine.PlaceCall("startradio");
            }
            catch(Exception ex) 
            {
                MessageBox.Show("Скайп не установлен или произошла ошибка взаимодействия с ним.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLogger.Instance.WriteError(ex);
            }
        }

       
       
    }
}
