﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using Common;

namespace RSPlay
{
    public partial class NewVersionForm : Form
    {
        public NewVersionForm()
        {
            InitializeComponent();
        }

        private void NewVersionForm_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate(Constants.UPDATE_URL);
        }
    }
}
