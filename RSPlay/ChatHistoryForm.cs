﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Common;

namespace RSPlay
{
    public partial class ChatHistoryForm : Form
    {
        public ChatHistoryForm()
        {
            InitializeComponent();
        }

        private void ChatHistoryForm_Load(object sender, EventArgs e)
        {
            try{
                string[] lines = File.ReadAllLines(Constants.CHAT_HISTORY_PATH);
                foreach (string line in lines)
                    richTextBoxEx1.AppendText(string.Format("{0}{1}", line, Environment.NewLine));
                richTextBoxEx1.ScrollToEnd();
            }catch
            {
            }
        }

        private void richTextBoxEx1_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Helper.OpenLink(e.LinkText);
        }
    }
}
