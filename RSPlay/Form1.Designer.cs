﻿using RSPlay.UserControls;
using System.Windows.Forms;
namespace RSPlay
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelPlayed = new RSPlay.UserControls.RichTextBoxEx();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBoxWhatOnlineDesc = new RSPlay.UserControls.RichTextBoxEx();
            this.buttonLastFMLike = new System.Windows.Forms.Button();
            this.buttonTwiLike = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.labelWhatOnlineSong = new System.Windows.Forms.Label();
            this.labelWhatOnlineCaption = new System.Windows.Forms.Label();
            this.pictureBoxWhatOnline = new System.Windows.Forms.PictureBox();
            this.doubleBufferedPanel1 = new RSPlay.UserControls.DoubleBufferedPanel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.checkBoxPrivat = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonSendMessage = new System.Windows.Forms.Button();
            this.textBoxChatSendMessage = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBoxChatMessages = new RSPlay.UserControls.RichTextBoxEx();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.richTextBoxChatUsers = new RSPlay.UserControls.RichTextBoxEx();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panelPrograms = new System.Windows.Forms.Panel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panelNews = new System.Windows.Forms.Panel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.историяЧатаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проксиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверятьНаОбновленияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.аккаунтВтвиттереToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lastfmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сообщитьОБагеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.чтоНовогоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemTwitter = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemIncludeNews = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemIncludeSong = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemShow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabelGlobalStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWhatOnline)).BeginInit();
            this.doubleBufferedPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(639, 265);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.ImageIndex = 0;
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(631, 238);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Эфир";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(625, 232);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.doubleBufferedPanel1);
            this.panel1.Location = new System.Drawing.Point(-3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 235);
            this.panel1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.labelPlayed);
            this.groupBox3.Controls.Add(this.richTextBoxWhatOnlineDesc);
            this.groupBox3.Controls.Add(this.buttonLastFMLike);
            this.groupBox3.Controls.Add(this.buttonTwiLike);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.labelWhatOnlineSong);
            this.groupBox3.Controls.Add(this.labelWhatOnlineCaption);
            this.groupBox3.Controls.Add(this.pictureBoxWhatOnline);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(631, 200);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сейчас в эфире:";
            // 
            // labelPlayed
            // 
            this.labelPlayed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.labelPlayed.ContextMenuStrip = this.contextMenuStrip3;
            this.labelPlayed.DetectUrls = true;
            this.labelPlayed.HiglightColor = RSPlay.UserControls.RtfColor.White;
            this.labelPlayed.Location = new System.Drawing.Point(266, 153);
            this.labelPlayed.Name = "labelPlayed";
            this.labelPlayed.ReadOnly = true;
            this.labelPlayed.Size = new System.Drawing.Size(205, 41);
            this.labelPlayed.TabIndex = 9;
            this.labelPlayed.Text = "";
            this.labelPlayed.TextColor = RSPlay.UserControls.RtfColor.Black;
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripSeparator3,
            this.toolStripMenuItem6});
            this.contextMenuStrip3.Name = "contextMenuStrip2";
            this.contextMenuStrip3.Size = new System.Drawing.Size(187, 54);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem4.Text = "Копировать в буфер";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(183, 6);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem6.Text = "Поиск в гугл (:";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // richTextBoxWhatOnlineDesc
            // 
            this.richTextBoxWhatOnlineDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxWhatOnlineDesc.DetectUrls = true;
            this.richTextBoxWhatOnlineDesc.HiglightColor = RSPlay.UserControls.RtfColor.White;
            this.richTextBoxWhatOnlineDesc.Location = new System.Drawing.Point(214, 36);
            this.richTextBoxWhatOnlineDesc.Name = "richTextBoxWhatOnlineDesc";
            this.richTextBoxWhatOnlineDesc.ReadOnly = true;
            this.richTextBoxWhatOnlineDesc.Size = new System.Drawing.Size(384, 109);
            this.richTextBoxWhatOnlineDesc.TabIndex = 8;
            this.richTextBoxWhatOnlineDesc.Text = "";
            this.richTextBoxWhatOnlineDesc.TextColor = RSPlay.UserControls.RtfColor.Black;
            // 
            // buttonLastFMLike
            // 
            this.buttonLastFMLike.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonLastFMLike.Image = global::RSPlay.Properties.Resources.favicon_2;
            this.buttonLastFMLike.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLastFMLike.Location = new System.Drawing.Point(540, 148);
            this.buttonLastFMLike.Name = "buttonLastFMLike";
            this.buttonLastFMLike.Size = new System.Drawing.Size(58, 23);
            this.buttonLastFMLike.TabIndex = 7;
            this.buttonLastFMLike.Text = "Like!";
            this.buttonLastFMLike.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonLastFMLike, "Лайкнуть в last.fm");
            this.buttonLastFMLike.UseVisualStyleBackColor = true;
            this.buttonLastFMLike.Click += new System.EventHandler(this.buttonLastFMLike_Click);
            // 
            // buttonTwiLike
            // 
            this.buttonTwiLike.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTwiLike.Image = global::RSPlay.Properties.Resources.twitter;
            this.buttonTwiLike.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTwiLike.Location = new System.Drawing.Point(477, 148);
            this.buttonTwiLike.Name = "buttonTwiLike";
            this.buttonTwiLike.Size = new System.Drawing.Size(53, 23);
            this.buttonTwiLike.TabIndex = 5;
            this.buttonTwiLike.Text = "Like!";
            this.buttonTwiLike.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonTwiLike, "Лайкнуть в twitter");
            this.buttonTwiLike.UseVisualStyleBackColor = true;
            this.buttonTwiLike.Click += new System.EventHandler(this.buttonLike_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(263, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 4;
            // 
            // labelWhatOnlineSong
            // 
            this.labelWhatOnlineSong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelWhatOnlineSong.AutoSize = true;
            this.labelWhatOnlineSong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWhatOnlineSong.Location = new System.Drawing.Point(207, 153);
            this.labelWhatOnlineSong.Name = "labelWhatOnlineSong";
            this.labelWhatOnlineSong.Size = new System.Drawing.Size(53, 13);
            this.labelWhatOnlineSong.TabIndex = 3;
            this.labelWhatOnlineSong.Text = "Играет:";
            // 
            // labelWhatOnlineCaption
            // 
            this.labelWhatOnlineCaption.AutoSize = true;
            this.labelWhatOnlineCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.labelWhatOnlineCaption.Location = new System.Drawing.Point(211, 16);
            this.labelWhatOnlineCaption.Name = "labelWhatOnlineCaption";
            this.labelWhatOnlineCaption.Size = new System.Drawing.Size(218, 16);
            this.labelWhatOnlineCaption.TabIndex = 1;
            this.labelWhatOnlineCaption.Text = "Проигрывание остановлено";
            // 
            // pictureBoxWhatOnline
            // 
            this.pictureBoxWhatOnline.Image = global::RSPlay.Properties.Resources.logo;
            this.pictureBoxWhatOnline.Location = new System.Drawing.Point(6, 36);
            this.pictureBoxWhatOnline.Name = "pictureBoxWhatOnline";
            this.pictureBoxWhatOnline.Size = new System.Drawing.Size(199, 109);
            this.pictureBoxWhatOnline.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxWhatOnline.TabIndex = 0;
            this.pictureBoxWhatOnline.TabStop = false;
            // 
            // doubleBufferedPanel1
            // 
            this.doubleBufferedPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.doubleBufferedPanel1.Controls.Add(this.button2);
            this.doubleBufferedPanel1.Controls.Add(this.trackBar1);
            this.doubleBufferedPanel1.Controls.Add(this.button1);
            this.doubleBufferedPanel1.Controls.Add(this.buttonStop);
            this.doubleBufferedPanel1.Controls.Add(this.buttonPlay);
            this.doubleBufferedPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.doubleBufferedPanel1.Location = new System.Drawing.Point(0, 200);
            this.doubleBufferedPanel1.Name = "doubleBufferedPanel1";
            this.doubleBufferedPanel1.Size = new System.Drawing.Size(631, 35);
            this.doubleBufferedPanel1.TabIndex = 4;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(204, 7);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(124, 45);
            this.trackBar1.TabIndex = 3;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // button1
            // 
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(349, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "На сайт";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Image = ((System.Drawing.Image)(resources.GetObject("buttonStop.Image")));
            this.buttonStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStop.Location = new System.Drawing.Point(100, 6);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 1;
            this.buttonStop.Text = "Стоп";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Image = ((System.Drawing.Image)(resources.GetObject("buttonPlay.Image")));
            this.buttonPlay.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPlay.Location = new System.Drawing.Point(8, 6);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(76, 23);
            this.buttonPlay.TabIndex = 0;
            this.buttonPlay.Text = "Старт";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.ImageIndex = 1;
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(631, 238);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Чат";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.checkBoxPrivat);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel1.Controls.Add(this.buttonSendMessage);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxChatSendMessage);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(625, 232);
            this.splitContainer1.SplitterDistance = 512;
            this.splitContainer1.TabIndex = 1;
            // 
            // checkBoxPrivat
            // 
            this.checkBoxPrivat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPrivat.AutoSize = true;
            this.checkBoxPrivat.Enabled = false;
            this.checkBoxPrivat.Location = new System.Drawing.Point(435, 207);
            this.checkBoxPrivat.Name = "checkBoxPrivat";
            this.checkBoxPrivat.Size = new System.Drawing.Size(55, 17);
            this.checkBoxPrivat.TabIndex = 5;
            this.checkBoxPrivat.Text = "лично";
            this.checkBoxPrivat.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 203);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Включить/выключить звук");
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // buttonSendMessage
            // 
            this.buttonSendMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSendMessage.Location = new System.Drawing.Point(352, 203);
            this.buttonSendMessage.Name = "buttonSendMessage";
            this.buttonSendMessage.Size = new System.Drawing.Size(77, 23);
            this.buttonSendMessage.TabIndex = 2;
            this.buttonSendMessage.Text = "Отправить";
            this.buttonSendMessage.UseVisualStyleBackColor = true;
            this.buttonSendMessage.Click += new System.EventHandler(this.buttonSendMessage_Click);
            // 
            // textBoxChatSendMessage
            // 
            this.textBoxChatSendMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxChatSendMessage.Location = new System.Drawing.Point(35, 205);
            this.textBoxChatSendMessage.Name = "textBoxChatSendMessage";
            this.textBoxChatSendMessage.Size = new System.Drawing.Size(311, 20);
            this.textBoxChatSendMessage.TabIndex = 1;
            this.textBoxChatSendMessage.TextChanged += new System.EventHandler(this.textBoxChatSendMessage_TextChanged);
            this.textBoxChatSendMessage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxChatSendMessage_KeyDown);
            this.textBoxChatSendMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxChatSendMessage_KeyPress);
            this.textBoxChatSendMessage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxChatSendMessage_KeyUp);
            this.textBoxChatSendMessage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.textBoxChatSendMessage_MouseDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.richTextBoxChatMessages);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 192);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сообщения (ctrl + колесико мыши для изменения размера шрифта)";
            // 
            // richTextBoxChatMessages
            // 
            this.richTextBoxChatMessages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBoxChatMessages.ContextMenuStrip = this.contextMenuStrip2;
            this.richTextBoxChatMessages.DetectUrls = true;
            this.richTextBoxChatMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxChatMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.richTextBoxChatMessages.HiglightColor = RSPlay.UserControls.RtfColor.White;
            this.richTextBoxChatMessages.Location = new System.Drawing.Point(3, 16);
            this.richTextBoxChatMessages.Name = "richTextBoxChatMessages";
            this.richTextBoxChatMessages.ReadOnly = true;
            this.richTextBoxChatMessages.Size = new System.Drawing.Size(503, 173);
            this.richTextBoxChatMessages.TabIndex = 0;
            this.richTextBoxChatMessages.Text = "";
            this.richTextBoxChatMessages.TextColor = RSPlay.UserControls.RtfColor.Black;
            this.richTextBoxChatMessages.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextBoxChatMessages_LinkClicked);
            this.richTextBoxChatMessages.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.richTextBoxChatMessages_MouseDoubleClick);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripSeparator2,
            this.toolStripMenuItem3});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(291, 76);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(290, 22);
            this.toolStripMenuItem1.Text = "Копировать в буфер";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(290, 22);
            this.toolStripMenuItem2.Text = "Копировать в строку ввода сообщений";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(287, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(290, 22);
            this.toolStripMenuItem3.Text = "Поиск в гугл (:";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.richTextBoxChatUsers);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(109, 232);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Пользователи";
            // 
            // richTextBoxChatUsers
            // 
            this.richTextBoxChatUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBoxChatUsers.DetectUrls = true;
            this.richTextBoxChatUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxChatUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.richTextBoxChatUsers.HiglightColor = RSPlay.UserControls.RtfColor.White;
            this.richTextBoxChatUsers.Location = new System.Drawing.Point(3, 16);
            this.richTextBoxChatUsers.Name = "richTextBoxChatUsers";
            this.richTextBoxChatUsers.ReadOnly = true;
            this.richTextBoxChatUsers.Size = new System.Drawing.Size(103, 213);
            this.richTextBoxChatUsers.TabIndex = 1;
            this.richTextBoxChatUsers.Text = "";
            this.richTextBoxChatUsers.TextColor = RSPlay.UserControls.RtfColor.Black;
            this.richTextBoxChatUsers.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.richTextBoxChatUsers_MouseDoubleClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panelPrograms);
            this.tabPage3.ImageIndex = 2;
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(631, 238);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Расписание передач";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panelPrograms
            // 
            this.panelPrograms.AutoScroll = true;
            this.panelPrograms.AutoSize = true;
            this.panelPrograms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrograms.Location = new System.Drawing.Point(0, 0);
            this.panelPrograms.Name = "panelPrograms";
            this.panelPrograms.Size = new System.Drawing.Size(631, 238);
            this.panelPrograms.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panelNews);
            this.tabPage4.ImageIndex = 3;
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(631, 238);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Новости";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panelNews
            // 
            this.panelNews.AutoScroll = true;
            this.panelNews.AutoSize = true;
            this.panelNews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelNews.Location = new System.Drawing.Point(0, 0);
            this.panelNews.Name = "panelNews";
            this.panelNews.Size = new System.Drawing.Size(631, 238);
            this.panelNews.TabIndex = 1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "kmix[1].png");
            this.imageList1.Images.SetKeyName(1, "chat[1].png");
            this.imageList1.Images.SetKeyName(2, "notes[1].png");
            this.imageList1.Images.SetKeyName(3, "news[1].png");
            this.imageList1.Images.SetKeyName(4, "twitter.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem1,
            this.настройкаToolStripMenuItem,
            this.помощьToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(639, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem1
            // 
            this.файлToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.историяЧатаToolStripMenuItem,
            this.выходToolStripMenuItem1});
            this.файлToolStripMenuItem1.Name = "файлToolStripMenuItem1";
            this.файлToolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem1.Text = "Файл";
            // 
            // историяЧатаToolStripMenuItem
            // 
            this.историяЧатаToolStripMenuItem.Image = global::RSPlay.Properties.Resources._16px_History;
            this.историяЧатаToolStripMenuItem.Name = "историяЧатаToolStripMenuItem";
            this.историяЧатаToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.историяЧатаToolStripMenuItem.Text = "История чата";
            this.историяЧатаToolStripMenuItem.Click += new System.EventHandler(this.историяЧатаToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem1
            // 
            this.выходToolStripMenuItem1.Image = global::RSPlay.Properties.Resources.xfce_system_exit;
            this.выходToolStripMenuItem1.Name = "выходToolStripMenuItem1";
            this.выходToolStripMenuItem1.Size = new System.Drawing.Size(148, 22);
            this.выходToolStripMenuItem1.Text = "Выход";
            this.выходToolStripMenuItem1.Click += new System.EventHandler(this.выходToolStripMenuItem1_Click_1);
            // 
            // настройкаToolStripMenuItem
            // 
            this.настройкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.проксиToolStripMenuItem,
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem,
            this.проверятьНаОбновленияToolStripMenuItem,
            this.аккаунтВтвиттереToolStripMenuItem,
            this.lastfmToolStripMenuItem,
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem});
            this.настройкаToolStripMenuItem.Image = global::RSPlay.Properties.Resources.icon_settings;
            this.настройкаToolStripMenuItem.Name = "настройкаToolStripMenuItem";
            this.настройкаToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.настройкаToolStripMenuItem.Text = "Н&астройки";
            // 
            // проксиToolStripMenuItem
            // 
            this.проксиToolStripMenuItem.Image = global::RSPlay.Properties.Resources.test_connection;
            this.проксиToolStripMenuItem.Name = "проксиToolStripMenuItem";
            this.проксиToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.проксиToolStripMenuItem.Text = "Прокси";
            this.проксиToolStripMenuItem.Click += new System.EventHandler(this.проксиToolStripMenuItem_Click);
            // 
            // запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem
            // 
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.CheckOnClick = true;
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.Name = "запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem";
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.Text = "Запускать проигрывание при старте программы";
            this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem.Click += new System.EventHandler(this.запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem_Click);
            // 
            // проверятьНаОбновленияToolStripMenuItem
            // 
            this.проверятьНаОбновленияToolStripMenuItem.CheckOnClick = true;
            this.проверятьНаОбновленияToolStripMenuItem.Name = "проверятьНаОбновленияToolStripMenuItem";
            this.проверятьНаОбновленияToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.проверятьНаОбновленияToolStripMenuItem.Text = "Проверять на обновления";
            this.проверятьНаОбновленияToolStripMenuItem.Click += new System.EventHandler(this.проверятьНаОбновленияToolStripMenuItem_Click);
            // 
            // аккаунтВтвиттереToolStripMenuItem
            // 
            this.аккаунтВтвиттереToolStripMenuItem.CheckOnClick = true;
            this.аккаунтВтвиттереToolStripMenuItem.Image = global::RSPlay.Properties.Resources.twitter;
            this.аккаунтВтвиттереToolStripMenuItem.Name = "аккаунтВтвиттереToolStripMenuItem";
            this.аккаунтВтвиттереToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.аккаунтВтвиттереToolStripMenuItem.Text = "Аккаунт в твиттере";
            this.аккаунтВтвиттереToolStripMenuItem.Click += new System.EventHandler(this.аккаунтВтвиттереToolStripMenuItem_Click);
            // 
            // lastfmToolStripMenuItem
            // 
            this.lastfmToolStripMenuItem.CheckOnClick = true;
            this.lastfmToolStripMenuItem.Image = global::RSPlay.Properties.Resources.favicon_2;
            this.lastfmToolStripMenuItem.Name = "lastfmToolStripMenuItem";
            this.lastfmToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.lastfmToolStripMenuItem.Text = "Аккаунт в Last.fm";
            this.lastfmToolStripMenuItem.Click += new System.EventHandler(this.lastfmToolStripMenuItem_Click);
            // 
            // свернутьВТрейВместоЗакрытияToolStripMenuItem
            // 
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem.CheckOnClick = true;
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem.Name = "свернутьВТрейВместоЗакрытияToolStripMenuItem";
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem.Text = "Сворачивать в трей вместо закрытия";
            this.свернутьВТрейВместоЗакрытияToolStripMenuItem.Click += new System.EventHandler(this.свернутьВТрейВместоЗакрытияToolStripMenuItem_Click);
            // 
            // помощьToolStripMenuItem1
            // 
            this.помощьToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сообщитьОБагеToolStripMenuItem,
            this.оПрограммеToolStripMenuItem,
            this.оПрограммеToolStripMenuItem1,
            this.чтоНовогоToolStripMenuItem});
            this.помощьToolStripMenuItem1.Name = "помощьToolStripMenuItem1";
            this.помощьToolStripMenuItem1.Size = new System.Drawing.Size(93, 20);
            this.помощьToolStripMenuItem1.Text = "Информация";
            // 
            // сообщитьОБагеToolStripMenuItem
            // 
            this.сообщитьОБагеToolStripMenuItem.Image = global::RSPlay.Properties.Resources.envelope_gif;
            this.сообщитьОБагеToolStripMenuItem.Name = "сообщитьОБагеToolStripMenuItem";
            this.сообщитьОБагеToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.сообщитьОБагеToolStripMenuItem.Text = "Предложения по улучшению/сообщить об ошибке";
            this.сообщитьОБагеToolStripMenuItem.Click += new System.EventHandler(this.сообщитьОБагеToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Image = global::RSPlay.Properties.Resources.icon_browser_update;
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.оПрограммеToolStripMenuItem.Text = "Проверить обновления";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem1
            // 
            this.оПрограммеToolStripMenuItem1.Name = "оПрограммеToolStripMenuItem1";
            this.оПрограммеToolStripMenuItem1.Size = new System.Drawing.Size(363, 22);
            this.оПрограммеToolStripMenuItem1.Text = "О программе";
            this.оПрограммеToolStripMenuItem1.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem1_Click);
            // 
            // чтоНовогоToolStripMenuItem
            // 
            this.чтоНовогоToolStripMenuItem.Name = "чтоНовогоToolStripMenuItem";
            this.чтоНовогоToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.чтоНовогоToolStripMenuItem.Text = "Что нового";
            this.чтоНовогоToolStripMenuItem.Click += new System.EventHandler(this.чтоНовогоToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemTwitter,
            this.toolStripMenuItemIncludeNews,
            this.toolStripMenuItemIncludeSong});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "&Настройки";
            // 
            // toolStripMenuItemTwitter
            // 
            this.toolStripMenuItemTwitter.Name = "toolStripMenuItemTwitter";
            this.toolStripMenuItemTwitter.Size = new System.Drawing.Size(257, 22);
            this.toolStripMenuItemTwitter.Text = "Подключить &твиттер";
            // 
            // toolStripMenuItemIncludeNews
            // 
            this.toolStripMenuItemIncludeNews.CheckOnClick = true;
            this.toolStripMenuItemIncludeNews.Name = "toolStripMenuItemIncludeNews";
            this.toolStripMenuItemIncludeNews.Size = new System.Drawing.Size(257, 22);
            this.toolStripMenuItemIncludeNews.Text = "&Уведомлять о новостях в трее";
            // 
            // toolStripMenuItemIncludeSong
            // 
            this.toolStripMenuItemIncludeSong.CheckOnClick = true;
            this.toolStripMenuItemIncludeSong.Name = "toolStripMenuItemIncludeSong";
            this.toolStripMenuItemIncludeSong.Size = new System.Drawing.Size(257, 22);
            this.toolStripMenuItemIncludeSong.Text = "Уведомлять о новой &песне в трее";
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAbout});
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.помощьToolStripMenuItem.Text = "&Помощь";
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItemAbout.Text = "&О программе";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Ф&айл";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Готово";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 289);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(639, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(112, 17);
            this.toolStripStatusLabel.Text = "toolStripStatusLabel";
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "RadioStart";
            this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.notifyIcon_DoubleClick);
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemShow,
            this.toolStripSeparator1,
            this.toolStripMenuItemExit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 54);
            // 
            // toolStripMenuItemShow
            // 
            this.toolStripMenuItemShow.Image = global::RSPlay.Properties.Resources.logo;
            this.toolStripMenuItemShow.Name = "toolStripMenuItemShow";
            this.toolStripMenuItemShow.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemShow.Text = "Показать";
            this.toolStripMenuItemShow.Click += new System.EventHandler(this.toolStripMenuItemShow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Image = global::RSPlay.Properties.Resources.xfce_system_exit;
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemExit.Text = "Выход";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // toolStripStatusLabelGlobalStatus
            // 
            this.toolStripStatusLabelGlobalStatus.Image = global::RSPlay.Properties.Resources.done;
            this.toolStripStatusLabelGlobalStatus.Name = "toolStripStatusLabelGlobalStatus";
            this.toolStripStatusLabelGlobalStatus.Size = new System.Drawing.Size(61, 17);
            this.toolStripStatusLabelGlobalStatus.Text = "Готово";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Image = global::RSPlay.Properties.Resources.done;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(128, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel";
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(477, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Звонок в эфир";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_3);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 311);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(655, 349);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RadioStart Player";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.contextMenuStrip3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWhatOnline)).EndInit();
            this.doubleBufferedPanel1.ResumeLayout(false);
            this.doubleBufferedPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonSendMessage;
        private System.Windows.Forms.TextBox textBoxChatSendMessage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTwitter;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemIncludeNews;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemIncludeSong;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelGlobalStatus;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemShow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem настройкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem аккаунтВтвиттереToolStripMenuItem;
        private System.Windows.Forms.Panel panelPrograms;
        private System.Windows.Forms.Panel panelNews;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сообщитьОБагеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem запускатьПроигрываниеПриСтартеПрограммыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверятьНаОбновленияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem свернутьВТрейВместоЗакрытияToolStripMenuItem;
        private RichTextBoxEx richTextBoxChatUsers;
        private ToolStripMenuItem историяЧатаToolStripMenuItem;
        private ToolStripMenuItem проксиToolStripMenuItem;
        private ToolStripMenuItem чтоНовогоToolStripMenuItem;
        private ToolStripMenuItem lastfmToolStripMenuItem;
        private ContextMenuStrip contextMenuStrip2;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem toolStripMenuItem3;
        private PictureBox pictureBox1;
        private Panel panel2;
        private Panel panel1;
        private GroupBox groupBox3;
        private Button buttonLastFMLike;
        private Button buttonTwiLike;
        private Label label3;
        private Label labelWhatOnlineSong;
        private Label labelWhatOnlineCaption;
        private PictureBox pictureBoxWhatOnline;
        private DoubleBufferedPanel doubleBufferedPanel1;
        private TrackBar trackBar1;
        private Button button1;
        private Button buttonStop;
        private Button buttonPlay;
        private RichTextBoxEx richTextBoxWhatOnlineDesc;
        private RichTextBoxEx labelPlayed;
        private GroupBox groupBox1;
        private RichTextBoxEx richTextBoxChatMessages;
        private ToolTip toolTip1;
        private CheckBox checkBoxPrivat;
        private ContextMenuStrip contextMenuStrip3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem toolStripMenuItem6;
        private Button button2;
    }
}

