﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;

namespace RSPlay
{
    public partial class ChatLoginForm : Form
    {
        public string Nick {
            get { return textBox1.Text; }
        }
        RadioStartChatEngine chat;

        public ChatLoginForm(RadioStartChatEngine chat)
        {
            InitializeComponent();
            this.chat = chat;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //if (chat.Enter() == false) 
            //{
            
            //}
            switch (chat.Enter(textBox1.Text)) 
            {
                case ChatState.IS_USED:
                    MessageBox.Show("Это имя уже используется. Введите другое имя", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case ChatState.SOME_ERROR:
                case ChatState.NEED_CONNECTED:
                default:
                    MessageBox.Show("Произошла ошибка при входе в чат. Попробуйте позже", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case ChatState.OK:
                    break;
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1_Click(sender, e);
        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            textBox1_KeyDown(sender, e);
        }
    }
}
