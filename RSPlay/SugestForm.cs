﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using Common;

namespace RSPlay
{
    public partial class SugestForm : Form
    {

        public SugestForm()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                MailSender.Instance.Subject = string.Format("{0} {1}",comboBox1.SelectedItem, SettingInfo.Instance.SettingData.MachineIdentifier);
                MailSender.Instance.Message = textBox1.Text;
                MailSender.Instance.SendMailLocal();
                MessageBox.Show("Ваше сообщение отправлено!","Обратная связь",MessageBoxButtons.OK,MessageBoxIcon.Information);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch(Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
                MessageBox.Show("Ошибка при отправки сообщения((","Ошибка",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            
        }

        private void SugestForm_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

    }
}
