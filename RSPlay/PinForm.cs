﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RSPlay
{
    public partial class PinForm : Form
    {
        public string Pin {
            get { return textBox1.Text; }
        }
        public PinForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void PinForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1_Click(null, null);
        }

        private void PinForm_Load(object sender, EventArgs e)
        {
            button1.KeyDown += new KeyEventHandler(PinForm_KeyDown);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            PinForm_KeyDown(sender, e);
        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            PinForm_KeyDown(sender, e);
        }
    }
}
