﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading;
using Un4seen.Bass;
using System.IO;
using Common;

namespace RSPlay.Engine
{
    public delegate void PlayerChannelStateEvent(bool isActive);

    class RadioPlayerEngine 
    {
        private const string SERVER_URL = Constants.RadioStartSite.SERVER_STREAM_URL;
        public event ChangeTitle OnChangeTitle;
        private int channelHandle = -1;
        private bool played = false;
        PulseThread getDataThread;

        public bool Played
        {
            get { return played; }
        }

        private string title = string.Empty;
        public string Title { get {
            return title; } }

        public event PlayerChannelStateEvent OnPlayerCheckState;

        class FXData 
        {
            public BASSFXType Effect;
            public int Handle;
        }

        List<FXData> fxs  = new List<FXData>();

        public RadioPlayerEngine() 
        {
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            getDataThread = new PulseThread(GetDataThread, 5);
            OnChangeTitle += new ChangeTitle(RadioPlayer_OnChangeTitle);
            OnPlayerCheckState += new PlayerChannelStateEvent(RadioPlayer_OnPlayerCheckState);
        }

        void RadioPlayer_OnPlayerCheckState(bool isActive)
        {
        }

        void RadioPlayer_OnChangeTitle(string title)
        {
           
        }

        public void SetVolume(int volume) 
        {
            
            Bass.BASS_ChannelSetAttribute(channelHandle, BASSAttribute.BASS_ATTRIB_VOL, (float)volume / 100);
        }

        public int GetVolume() 
        {
            return (int)(Bass.BASS_GetVolume() * 100.0);
        }
 
        public bool Play()
        {
            played = false;
            try
            {

                Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT | BASSInit.BASS_DEVICE_3D, IntPtr.Zero);
                int stream = Bass.BASS_StreamCreateURL(SERVER_URL, 0, BASSFlag.BASS_DEFAULT | BASSFlag.BASS_AAC_STEREO, null, IntPtr.Zero);
                if (stream != 0)
                {
                    played = true;
                    SetVolume(GetVolume());
                    Bass.BASS_ChannelPlay(stream, false);
                    channelHandle = stream;
                }
                else
                {

                    Console.WriteLine("Stream error: {0}", Bass.BASS_ErrorGetCode());
                }

                getDataThread.Start();
            }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
                played = false;
                getDataThread.Stop();
            }
            return played;
        }

        private PulseState GetDataThread(object data)
        {
                
                FoundTag();
                return PulseState.CONTINUE;
        }

        public void SetEffect(BASSFXType basseffect) 
        {
           int handle = Bass.BASS_ChannelSetFX(channelHandle, basseffect, 0);
           if (handle != -1)
               fxs.Add(new FXData() { Effect = basseffect, Handle = handle });
        }

        public void RemoveEffect(BASSFXType basseffect)
        {
            FXData fx = fxs.Find(x => x.Effect == basseffect);
            if (fx == null)
                return;
            Bass.BASS_ChannelRemoveFX(channelHandle, fx.Handle);
        }

        private void FoundTag()
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;

            int metaInt = 0;
            int count = 0;
            int metadataLength = 0;
            string metadataHeader = "";
            string oldMetadataHeader = null;

            byte[] buffer = new byte[512];

            Stream socketStream = null;
            Stream byteOut = null;

            request = (HttpWebRequest)WebRequest.Create(SERVER_URL);
            String serverPath = "/";

            request.Headers.Clear();
            request.Headers.Add("GET", serverPath + " HTTP/1.0");
            request.Headers.Add("Icy-MetaData", "1");
            request.UserAgent = "WinampMPEG/5.09";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            metaInt = Convert.ToInt32(response.GetResponseHeader("icy-metaint"));

            try
            {
                socketStream = response.GetResponseStream();
                while (played)
                {
                    int bufLen = socketStream.Read(buffer, 0, buffer.Length);
                    if (bufLen < 0)
                        return;

                    for (int i = 0; i < bufLen; i++)
                    {
                        if (metadataLength != 0)
                        {
                            metadataHeader += Convert.ToChar(buffer[i]);
                            metadataLength--;
                            if (metadataLength == 0)
                            {

                                if (!metadataHeader.Equals(oldMetadataHeader))
                                {
                                    if (byteOut != null)
                                    {
                                        //byteOut.Flush();
                                        //byteOut.Close();
                                    }

                                    if (metadataHeader.Contains("StreamTitle="))
                                    {
                                        title = Encoding.GetEncoding(1251).GetString(Encoding.GetEncoding(28591).GetBytes(metadataHeader.Substring("StreamTitle=".Length).Trim()));
                                        title = title.Replace("'", string.Empty).Replace(";", string.Empty).Replace("\0", string.Empty);
                                        OnChangeTitle(title);
                                        return;
                                    }

                                    //byteOut = createNewFile(destPath, fileName);

                                    oldMetadataHeader = metadataHeader;

                                }
                                metadataHeader = "";
                            }
                        }
                        else
                        {
                            if (count++ < metaInt)
                            {
                                if (byteOut != null)
                                {
                                    //byteOut.Write(buffer, i, 1);
                                    //if (count % 100 == 0)
                                    //    byteOut.Flush();
                                }
                            }
                            else
                            {
                                metadataLength = Convert.ToInt32(buffer[i]) * 16;
                                count = 0;
                            }
                        }
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
                played = false;

            }
            finally
            {
                if (byteOut != null)
                    byteOut.Close();
                if (socketStream != null)
                    socketStream.Close();
            }
        }

        public void Stop()
        {
            played = false;
            try
            {
                getDataThread.Stop();
                Bass.BASS_StreamFree(channelHandle);
                // free BASS  
                Bass.BASS_Free();
                OnPlayerCheckState(false);
            }
            catch
            {

            }

            
        }

        public void Record() {
            //Bass.BASS_RecordStart(44100,stream,BASSFlag.BASS_DEFAULT,new RECORDPROC(
        }

    }

}
