﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Common;

namespace RSPlay.Engine
{
    class SettingInfo : SingletonBased<SettingInfo>
    {
        private SettingData settingData = new SettingData();

        private static readonly string identifier = String.Format("{0} {1}", Environment.MachineName, Environment.MachineName);

        public static string Identifier
        {
            get { return identifier; }
        }
        public bool WasNotExist = false;
        private SettingInfo()  
        {
            try
            {
                Load();
            }
            catch 
            {
                Save();
            }
        }

        public virtual SettingData SettingData
        {
            get { return settingData; }
            set { settingData = value; }
        }

        private static readonly string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.SETTING_FILE);

        public virtual bool Load(string file = null)
        {
            string path = file == null ? settingFile : file;
            if (!File.Exists(path))
            {
                WasNotExist = true;
                return false;
            }
            var settings = ObjectSerializer<SettingData>.LoadObjectFromFile(path);

            if (settings.MachineIdentifier != identifier)
            {

                return false;
            }

            settingData = settings;

            return true;
        }
        public override string ToString()
        {
            return string.Format("Machine: {0};{1}Version:{2}{1};Time:{3}{1}", SettingData.MachineIdentifier, Environment.NewLine,
                SettingData.CurrentVersion, SettingData.LastTime);
        }
        public virtual void Save(string file = null)
        {
            string path = file == null ? settingFile : file;

            ObjectSerializer<SettingData>.SaveObjectToFile(settingData, path);
        }

        public bool Loaded { get { 
            return settingData.MachineIdentifier == identifier; } }
    }

    [Serializable]
    public class SettingData
    {
        public DateTime LastTime = DateTime.Now;
        public string CurrentVersion = Constants.APP_NAME;
        public string MachineIdentifier = SettingInfo.Identifier;
        public AccountInfo Account = new AccountInfo();
        public string DeskFile = string.Empty;
        public bool CheckUpdate = true;
        public bool AutoStartPlayer = true;
        public bool HideInsteadClose = false;
        public string WebProxy = string.Empty;
        public bool UseProxy = false;
        public string LastFMSession = string.Empty;
        public bool EnableScrobble = false;
        public int ScrobbleValue = 5;
        public bool ChatSound = true;
        public int Volume = 100;
    }

    [Serializable]
    public class AccountInfo
    {
        public string User;
        public string Password;
    }

}
