﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace RSPlay.Engine
{
    class LastFMEngine :WebEngineBase
    {
        const string AUTH_URL = "http://ws.audioscrobbler.com/2.0/?method=auth.gettoken&api_key=";
        const string BROWSE_AUTH_TEMPLATE = "http://www.last.fm/api/auth/?api_key={0}&token={1}";
        
        const string SIGNATURE_TEMPLATE = "api_key{0}methodauth.getsessiontoken{1}{2}";//"api_key" + ApiKey + "methodauth.getsessiontoken" + token + mySecret;
        const string SESSION_URL_TEMPLATE = "http://ws.audioscrobbler.com/2.0/?method=auth.getsession&token={0}&api_key={1}&api_sig={2}";
        const string SCROBBLE_POST_DATA = "method={6}&sk={0}&api_key={1}&artist={2}&track={3}&timestamp={4}&api_sig={5}";
        const string SCROBBLE_API_SIG_DATA = "api_key{0}artist{1}method{6}sk{2}timestamp{3}track{4}{5}";

        const string SCROBBLE_URL = "http://ws.audioscrobbler.com/2.0/";
        string sessionKey;

        public string SessionKey
        {
            get { return sessionKey; }
            set { sessionKey = value; }
        }

        public bool Authorize() 
        {
            bool res = false;

            try
            {
            //    MessageBox.Show("Сейчас откроется окно браузера в котором необходимо подтвердить авторизацию приложения в Last.fm. По окончанию авторизации введите пин в появившимся окне.", "Авторизация в Last.fm", MessageBoxButtons.OK,
            //        MessageBoxIcon.Information);

                string result = RawGet(AUTH_URL + Constants.LastFM.CONSUMER_KEY, null, true);

                Match tokenMatch = Regex.Match(result, "token>(.*?)</token");
                if (!tokenMatch.Success)
                    return res;

                string token = tokenMatch.Groups[1].Value;

                Helper.OpenLink(string.Format(BROWSE_AUTH_TEMPLATE, Constants.LastFM.CONSUMER_KEY, token));

                DialogResult d = MessageBox.Show("Вы подтвердили доступ?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (d == DialogResult.OK)
                {
                    string tmp = string.Format(SIGNATURE_TEMPLATE, Constants.LastFM.CONSUMER_KEY, token, Constants.LastFM.CONSUMER_SECRET);
                    string sig = getMd5Hash(tmp);

                    string session = RawGet(string.Format(SESSION_URL_TEMPLATE, token, Constants.LastFM.CONSUMER_KEY, sig), string.Empty, true);
                    Match keyMatch = Regex.Match(session, "key>(.*?)</key");
                    if (!keyMatch.Success)
                        return res;
                    sessionKey = keyMatch.Groups[1].Value;
                    res = true;
                }
            }
            catch (Exception ex) 
            {
                ErrorLogger.Instance.WriteError(ex);
            }
            return res;
        }

        static string getMd5Hash(string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
           
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public bool Scrobble(string title) 
        {
            string[] splitted = title.Split('-');
            if (splitted.Length < 2)
                return false;
            string artist = splitted[0].Trim();
            string track = title.Substring(title.IndexOf('-') + 1).Trim();
            return Scrobble(artist, track);
        }

        public bool Love(string title)
        {
            string[] splitted = title.Split('-');
            if (splitted.Length < 2)
                return false;
            string artist = splitted[0].Trim();
            string track = title.Substring(title.IndexOf('-') + 1).Trim();
            return Love(artist, track);
        }

        public bool Scrobble(string artist, string track) 
        {
            return PushData(artist, track, "track.scrobble");
        }

        public bool Love(string artist, string track)
        {
            return PushData(artist, track, "track.love");
        }

        bool PushData(string artist, string track,string method) 
        {
            bool res = false;
            try
            {
                TimeSpan rtime = DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0));
                int timestamp = (int)rtime.TotalSeconds;
                //            const string SCROBBLE_POST_DATA = "method=track.scrobble&sk={0}&api_key={1}&artist={2}&track={3}&timestamp={4}&api_sig={5}";
                //const string SCROBBLE_API_SIG_DATA = "api_key{0}artist{1}methodtrack.scrobblesk{2}timestamp{3}track{4}{5}";

                string apiSig = string.Format(SCROBBLE_API_SIG_DATA, Constants.LastFM.CONSUMER_KEY, artist, sessionKey, timestamp, track, Constants.LastFM.CONSUMER_SECRET, method);

                string submissionReqString = string.Format(SCROBBLE_POST_DATA, sessionKey, Constants.LastFM.CONSUMER_KEY, artist, track, timestamp, getMd5Hash(apiSig),method);


                HttpWebRequest submissionRequest = (HttpWebRequest)WebRequest.Create(SCROBBLE_URL); // адрес запроса без параметров

                // очень важная строка. Долго я мучался, пока не выяснил, что она обязательно должна быть
                submissionRequest.ServicePoint.Expect100Continue = false;

                // Настраиваем параметры запроса
                submissionRequest.UserAgent = "Mozilla/5.0";
                // Указываем метод отправки данных скрипту, в случае с POST обязательно
                submissionRequest.Method = "POST";
                // В случае с POST обязательная строка
                submissionRequest.ContentType = "application/x-www-form-urlencoded";

                // ставим таймаут, чтобы программа не повисла при неудаче обращения к серверу, а выкинула Exception
                submissionRequest.Timeout = 6000;

                // Преобразуем данные в соответствующую кодировку, получаем массив байтов из строки с параметрами (UTF8 обязательно)
                byte[] EncodedPostParams = Encoding.UTF8.GetBytes(submissionReqString);
                submissionRequest.ContentLength = EncodedPostParams.Length;

                // Записываем данные в поток запроса (массив байтов, откуда начинаем, сколько записываем)
                submissionRequest.GetRequestStream().Write(EncodedPostParams, 0, EncodedPostParams.Length);
                // закрываем поток
                submissionRequest.GetRequestStream().Close();

                // получаем ответ сервера
                HttpWebResponse submissionResponse = (HttpWebResponse)submissionRequest.GetResponse();

                // считываем поток ответа
                string submissionResult = new StreamReader(submissionResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                // разбор полётов. Если ответ не содержит status="ok", то дело плохо, выкидываем Exception и где-нибудь ловим его.
                if (!submissionResult.Contains("status=\"ok\""))
                    throw new Exception("Треки не отправлены! Причина - " + submissionResult);
                res = true;
            }
            catch (Exception ex) 
            {
                ErrorLogger.Instance.WriteError(ex);
            }
            return res;
        }
    }
}
