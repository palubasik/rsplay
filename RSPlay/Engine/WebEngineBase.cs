﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;

namespace RSPlay.Engine
{
    public class WebEngineBase
    {
        protected static string webProxy;

        public static string GlobalWebProxy
        {
            get { return webProxy; }
            set { webProxy = value; }
        }

        private WebRequest lastRequest;

        public WebRequest LastRequest
        {
            get { return lastRequest; }
        }

        private WebResponse lastResponse;

        public WebResponse LastResponse
        {
            get { return lastResponse; }
        }
        
        public Encoding ResponseEncoding = Encoding.UTF8;

        public string RawGet(string url, string myCoo, bool redirect)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            if (!string.IsNullOrEmpty(myCoo))
            {
                request.Headers["Cookie"] = myCoo;
            }
            request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.64 Safari/532.5";
            request.AllowAutoRedirect = redirect;
            if (!string.IsNullOrEmpty(webProxy))
            {
                WebProxy proxy = new WebProxy();
                Uri uri = new Uri(webProxy);
                proxy.Address = uri;
                request.Proxy = proxy;
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), ResponseEncoding);
            lastRequest = request;
            lastResponse = response;
            return reader.ReadToEnd();
        }

        public string RawGetFile(string url, string myCoo)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            if (!string.IsNullOrEmpty(myCoo))
            {
                request.Headers["Cookie"] = myCoo;
            }
            request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.64 Safari/532.5";
            request.AllowAutoRedirect = false;
            if (!string.IsNullOrEmpty(webProxy))
            {
                WebProxy proxy = new WebProxy();
                Uri uri = new Uri(webProxy);
                proxy.Address = uri;
                request.Proxy = proxy;
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), ResponseEncoding);
            lastRequest = request;
            lastResponse = response;
            return reader.ReadToEnd();
        }

        public string RawPost(string url, string pData, string myCoo, bool redirect)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            if (!string.IsNullOrEmpty(myCoo))
            {
                request.Headers["Cookie"] = myCoo;
            }
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.64 Safari/532.5";
            request.AllowAutoRedirect = redirect;
            if (!string.IsNullOrEmpty(webProxy))
            {
                WebProxy proxy = new WebProxy();
                Uri uri = new Uri(webProxy);
                proxy.Address = uri;
                request.Proxy = proxy;
            }
            byte[] bytes = Encoding.UTF8.GetBytes(pData);
            request.ContentLength = bytes.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), ResponseEncoding);
            string res = reader.ReadToEnd();
            lastRequest = request;
            lastResponse = response;
            return res;
        }

        public string RawPostFile(string url, string pData, string myCoo, string contentType)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            if (!string.IsNullOrEmpty(myCoo))
            {
                request.Headers["Cookie"] = myCoo;
            }
            request.ContentType = contentType;
            request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.64 Safari/532.5";
            request.AllowAutoRedirect = false;
            if (!string.IsNullOrEmpty(webProxy))
            {
                WebProxy proxy = new WebProxy();
                Uri uri = new Uri(webProxy);
                proxy.Address = uri;
                request.Proxy = proxy;
            }
            byte[] bytes = Encoding.UTF8.GetBytes(pData);
            request.ContentLength = bytes.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), ResponseEncoding);
            lastRequest = request;
            lastResponse = response;
            return reader.ReadToEnd();
        }
    }

    
}
