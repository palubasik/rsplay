﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace RSPlay.Engine
{
    class ObjectSerializer<T>
    {
        public static bool SaveObjectToFile(T obj, string filename)
        {
            bool fOk = false;
            try
            {

                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, obj);
                stream.Close();
                fOk = true;
            }
            catch (Exception e)
            {
                throw new ObjectSerializerException("Error save to file", e);
            }

            return fOk;
        }

        public static T LoadObjectFromFile(string filename)
        {
            T obj;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                object objFromFile = formatter.Deserialize(stream);
                stream.Close();

                obj = (T)objFromFile;
            }
            catch (Exception e)
            {
                throw new ObjectSerializerException("Error load from file", e);
            }

            return obj;
        }
    }

    [Serializable]
    public class ObjectSerializerException : Exception
    {
        public ObjectSerializerException() :
            base("ObjectSerializer error.") { }

        public ObjectSerializerException(string message) :
            base(message) { }

        public ObjectSerializerException(string message, Exception innerException) :
            base(message, innerException) { }

        protected ObjectSerializerException(SerializationInfo info, StreamingContext context) :
            base(info, context) { }
    }


}
