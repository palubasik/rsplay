﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TweetSharp;
using System.Windows.Forms;
using Common;
using Un4seen.Bass;
using System.Text.RegularExpressions;

namespace RSPlay.Engine
{
    public delegate void UpdateProgramNews(List<RadioStartProgramData> data);
    public delegate void UpdateBlogNews(List<RadioStartNewsData> data);
    public delegate void ChatEvent(List<string> users, List<ChatMessageData> messages);
    public delegate void UpdateTweets(TwitterSearchResult results);
    public delegate void ChangeTitle(string title);
    public delegate void ChangeEvent();

    class GlobalEngine : SingletonBased<GlobalEngine>, IDisposable
    {
        private RadioStartChatEngine chat;
        private RadioStartParserEngine parser;
        private RadioPlayerEngine player;
        private SoftUpdaterEngine updater;
        private LastFMEngine lastfm;

        PulseThread programUpdaterThread = new PulseThread((x) => { return PulseState.ABORT; }, 60);

        PulseThread chatUpdaterThread = new PulseThread((x) => { return PulseState.ABORT; }, 60);
        PulseThread checkUpdateThread = new PulseThread((x) => { return PulseState.ABORT; }, 60);
        PulseThread checkTwitterThread = new PulseThread((x) => { return PulseState.ABORT; }, 60);
        PulseThread scrobbleThread =  new PulseThread((x) => { return PulseState.ABORT; }, 60);
        
        static object chatUpdateLock = new object();

        public event UpdateProgramNews OnProgramUpdate;
        public event UpdateBlogNews OnNewsUpdate;
        public event ChangeEvent OnBeginUpdate;
        public event ChangeEvent OnEndUpdate;
        public event ChangeTitle OnChangeTitle;
        public event ChatEvent OnChatUpdate;
        public event ChangeEvent OnUpdateNewVersion;
        public event UpdateTweets OnTweetsTake;

        public event PlayerChannelStateEvent OnPlayerCheckState;

        const int SCROBBLE_ITERATION_INTERVAL = 5;
        const int SCROBBLE_MAX_ITERATIONS = 4;
        const int PROGRAM_UPDATER_INTERVAL = 30;
        const int CHAT_UPDATER_INTERVAL = 2;
        const int SOFT_UPDATER_INTERVAL = 3;

        public string ChatNick
        {
            get { return chat.Nick; }
        }
        public GlobalEngine()
        {
            programUpdaterThread = new PulseThread(ProgramUpdaterWork, PROGRAM_UPDATER_INTERVAL);
            chatUpdaterThread = new PulseThread(ChatUpdateThreadWork, CHAT_UPDATER_INTERVAL);
            checkUpdateThread = new PulseThread(CheckForUpdateThreadWork, SOFT_UPDATER_INTERVAL);
          //  checkTwitterThread = new PulseThread(CheckTwitterThreadWork, 3);
            scrobbleThread = new PulseThread(ScrobbleThreadWork, SCROBBLE_ITERATION_INTERVAL);

            chat = new RadioStartChatEngine();
            parser = new RadioStartParserEngine();
            player = new RadioPlayerEngine();
            updater = new SoftUpdaterEngine();
            lastfm = new LastFMEngine();

            player.OnChangeTitle += new ChangeTitle(InvokeChangeTitle);
            player.OnPlayerCheckState += new PlayerChannelStateEvent(InvokePlayerChangeState);
            OnProgramUpdate += new UpdateProgramNews(EmptyProgramUpdate);
            OnNewsUpdate += new UpdateBlogNews(EmptyNewsUpdate);
            OnBeginUpdate += () => { };
            OnEndUpdate += () => { };
            OnUpdateNewVersion +=()=>{};
            OnChangeTitle += new ChangeTitle(EmptyChangeTitle);
            OnPlayerCheckState += new PlayerChannelStateEvent(EmptyPlayerCheckState);
            OnChatUpdate += new ChatEvent(EmptyChatUpdate);
            OnTweetsTake +=new UpdateTweets(EmptyTweetTake);
            lastfm.SessionKey = SettingInfo.Instance.SettingData.LastFMSession;
        }

        public void StartAutomaticUpdate() 
        {
            checkUpdateThread.Start();
        }
        public void StartTwitterUpdate() 
        {
           // checkTwitterThread.Start();
        }
        void EmptyChatUpdate(List<string> users, List<ChatMessageData> messages)
        {

        }

        public void EmptyTweetTake(TwitterSearchResult results) { }

        string lastTitle = string.Empty;

        void EmptyChangeTitle(string title)
        {
            if (SettingInfo.Instance.SettingData.EnableScrobble)
            {
                lastTitle = title;
            }
        }

        PulseState ScrobbleThreadWork(object data)
        {
            if (scrobbleThread == null)
                return PulseState.ABORT;

            object sdata = scrobbleThread.PopStateData();

            if (sdata == null)
            {
                scrobbleThread.PushStateData(new ScrobbleThreadStateData() { IterationCount = 0, Title = lastTitle });
                return PulseState.CONTINUE;
            }

            ScrobbleThreadStateData state = sdata as ScrobbleThreadStateData;
            
            if (state.Title == lastTitle)
                state.IterationCount++;
            else
            {
                state.IterationCount = 0;
                state.Title = lastTitle;
            }

            if (state.IterationCount == SettingInfo.Instance.SettingData.ScrobbleValue && !string.IsNullOrEmpty(state.Title) && Regex.Match(state.Title,@"\w").Success)
                lastfm.Scrobble(state.Title);

            scrobbleThread.PushStateData(state);

            return PulseState.CONTINUE;
        }

        void EmptyPlayerCheckState(bool isActive)
        {
        }

        void InvokePlayerChangeState(bool isActive)
        {
            OnPlayerCheckState(isActive);
        }

        void InvokeChangeTitle(string title)
        {
            OnChangeTitle(title);
        }

        private void EmptyNewsUpdate(List<RadioStartNewsData> data)
        {

        }

        private void EmptyProgramUpdate(List<RadioStartProgramData> data)
        {

        }

        private PulseState ProgramUpdaterWork(object data)
        {
            try
            {
                OnBeginUpdate();
                parser.ParsePrograms();
                OnProgramUpdate(parser.Programs);
                parser.ParseNews();
                OnNewsUpdate(parser.News);
                OnEndUpdate();
            }
            catch (ThreadAbortException) 
            {
            
            }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex, "Error while program update");
            }
            return PulseState.CONTINUE;
        }

        public void StartPlayer()
        {
            player.Play();
            if (SettingInfo.Instance.SettingData.EnableScrobble)
                StartScrobble();
        }

        public void StartNewsUpdater() 
        {
            programUpdaterThread.Start();
        }

        public void StopNewsUpdater() 
        {
            programUpdaterThread.Stop();
        }

        public void StartScrobble()
        {
            scrobbleThread.Start();
        }

        public void StopScrobble() 
        {
            scrobbleThread.Stop();
        }

        public bool SendLoveLastFM() 
        {
            if (!isAuthorizedLastfm())
            {
                if (MessageBox.Show("Вы не авторизованы в last.fm. Хотите авторизоваться?", "Авторизация в last.fm", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == DialogResult.Yes)
                {
                    if (!LastfmAuthAction())
                        return false;
                }
                else
                    return false;
            }
            return lastfm.Love(player.Title);
        }
        public void StopPlayer()
        {
            player.Stop();
            scrobbleThread.Stop();
        }

        public void StartRecord(string path)
        {
            player.Record();
        }

        public void StopRecord()
        {

        }

        //private void StopProgramThread() 
        //{
        //    if (programUpdaterThread != null)
        //    {
        //        programUpdaterWork = false;
        //        programUpdaterThread.Join();
        //        programUpdaterThread = null;
        //    }
        //}

        //private void StopChatThread() 
        //{
        //    if (chatUpdaterThread != null)
        //    {
        //        chatUpdaterWork = false;
        //        chatUpdaterThread.Join();
        //        chatUpdaterThread = null;
        //    }

        //}
        public bool TwitterAuthAction()
        {
            bool res = false;
            MessageBox.Show("Сейчас откроется окно браузера в котором необходимо подтвердить авторизацию приложения в твиттер. По окончанию авторизации введите пин в появившимся окне.", "Авторизация в твиттер", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            TwitterService service = new TwitterService(Constants.Twitter.CONSUMER_KEY, Constants.Twitter.CONSUMER_SECRET);

            // Step 1 - Retrieve an OAuth Request Token
            OAuthRequestToken requestToken = service.GetRequestToken();

            // Step 2 - Redirect to the OAuth Authorization URL
            Uri uri = service.GetAuthorizationUri(requestToken);
            Helper.OpenLink(uri.ToString());

            while (true)
            {
                PinForm pf = new PinForm();
                pf.ShowDialog();
                if (pf.DialogResult != System.Windows.Forms.DialogResult.OK)
                    return res;
                
                var accessToken = service.GetAccessToken(requestToken, pf.Pin);
                service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
                if (service.Response == null || service.Response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Неправильный пин. Попробуйте еще раз");
                    continue;
                }
                SettingInfo.Instance.SettingData.Account.User = accessToken.Token;
                SettingInfo.Instance.SettingData.Account.Password = accessToken.TokenSecret;
                MessageBox.Show("Авторизация в твиттере завершена. Спасибо!", "Авторизация в твиттер", MessageBoxButtons.OK,
              MessageBoxIcon.Information);
                break;
            }

            res = true;

            return res;
        }

        public bool LastfmAuthAction() {
            if (lastfm.Authorize())
            {
                SettingInfo.Instance.SettingData.LastFMSession = lastfm.SessionKey;
                SettingInfo.Instance.SettingData.EnableScrobble = true;
                SettingInfo.Instance.Save();
                scrobbleThread.Start();
                return true;
            }
            return false;
        }
        public bool isAuthorizedInTwitter()
        {
            return !string.IsNullOrEmpty(SettingInfo.Instance.SettingData.Account.User);
        }

        public bool isAuthorizedLastfm() 
        {
            return !string.IsNullOrEmpty(SettingInfo.Instance.SettingData.LastFMSession);
        }

        public bool SendTwiLike()
        {
            string message = "Мне падабаецца";
            message = AddWord(message, "#заразграе");
            message = AddWord(message, "#radiostart");  
            message = AddWord(message, player.Title.Trim());
            message = AddWord(message, ". Далучыцца праз RSPlay http://goo.gl/drxKg");
            return SendTwiMessage(message, true);
        }

        public bool SendRadioStartTwiMessage(string message, bool authIfNeed) 
        {
            message = AddWord(message, " #radiostart");
            return SendTwiMessage(message, authIfNeed);
        }

        public bool SendTwiMessage(string message, bool authIfNeed)
        {
            bool res = false;

            while (true)
            {
                if (string.IsNullOrEmpty(SettingInfo.Instance.SettingData.Account.User))
                    if (!authIfNeed || !TwitterAuthAction())
                        break;
                    else
                        Thread.Sleep(500);

                TwitterService service = new TwitterService(Constants.Twitter.CONSUMER_KEY, Constants.Twitter.CONSUMER_SECRET,
                    SettingInfo.Instance.SettingData.Account.User, SettingInfo.Instance.SettingData.Account.Password);

                service.SendTweet(message);
                if (service.Response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    if (authIfNeed && TwitterAuthAction())
                        continue;
                    else
                        break;
                }
                else if (service.Response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show(string.Format("Не получилось отослать сообщение в твиттер. Код ошибки {0}", service.Response.StatusCode));
                    break;
                }

                res = true;
                break;
            }
            return res;
        }


        public bool ChatLogin()
        {

            ChatLoginForm clf = new ChatLoginForm(chat);
            if (clf.ShowDialog() == DialogResult.OK)
            {
                OnChatUpdate(chat.Users, chat.Messages);
                chatUpdaterThread.Start();
                return true;
            }
            else
                return false;
        }

        public void ChatLogout() 
        {
            chat.SendCommand("quit", string.Empty);
        }

        public string ChatGetNick() 
        {
            return chat.Nick;
        }
        public bool ChatSendMessage(string message)
        {
            bool res = chat.SendMessage(message) == ChatState.OK;
            if (res)
                OnChatUpdate(chat.Users, chat.Messages);
            return res;
        }

        public bool ChatSendCommand(string message,string data)
        {
            bool res = chat.SendCommand(message,data) == ChatState.OK;
            if (res)
                OnChatUpdate(chat.Users, chat.Messages);
            return res;
        }

        public bool ChatSendPrivateMessage(string to, string message)
        {
            bool res = chat.SendPrivateMessage(to, message) == ChatState.OK;
            if (res)
                OnChatUpdate(chat.Users, chat.Messages);
            return res;
        }

        private PulseState ChatUpdateThreadWork(object data)
        {
            lock (chatUpdaterThread)
            {
                if (chat.ChatUpdate() == ChatState.OK)
                    OnChatUpdate(chat.Users, chat.Messages);
            }

            return PulseState.CONTINUE;
        }

        public int GetVolume() 
        {
            return player.GetVolume();
        }

        public void SetVolume(int volume) 
        {
            player.SetVolume(volume);
        }

        public bool ChatUpdate()
        {
            bool res = false;
            lock (chatUpdateLock)
            {
                ChatState cs = chat.ChatUpdate();
                if (cs == ChatState.OK)
                {
                    res = true;
                    OnChatUpdate(chat.Users, chat.Messages);
                }
                if (cs == ChatState.NO_CHANGES)
                    res = true;
            }
            return res;
        }
        private string AddWord(string message, string word)
        {
            if (message.Length + word.Length <= 140)
                message += " " + word;
            return message;
        }

        public void SetEffect(BASSFXType basseffect)
        {
            player.SetEffect(basseffect);
        }

        public void RemoveEffect(BASSFXType basseffect)
        {
            player.RemoveEffect(basseffect);
        }

        public void Dispose()
        {
            Helper.ExitProgram = true;
            ChatLogout();
            player.Stop();
            scrobbleThread.Stop();
            SettingInfo.Instance.Save();
            programUpdaterThread.Stop();
            chatUpdaterThread.Stop();
            checkUpdateThread.Stop();
            checkTwitterThread.Stop();
            Thread.Sleep(500);
        }

        PulseState CheckForUpdateThreadWork(object data)
        {
            if (!SettingInfo.Instance.SettingData.CheckUpdate)
                return PulseState.CONTINUE;
            if (CheckForUpdate())
                SettingInfo.Instance.SettingData.CheckUpdate = false;
            return PulseState.CONTINUE;
        }

       
        class ScrobbleThreadStateData 
        {
            public int IterationCount;
            public string Title;
        }
        public bool CheckForUpdate()
        {
            if (updater.IsNewVersion()) 
            {
                if (MessageBox.Show("Обнаружена новая версия программы. Выполнить автоматическое обновление?", Constants.APP_NAME, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    if (updater.Update())
                    {
                       
                       OnUpdateNewVersion();
                    }
                }
                return true;
            }
            return false;
        }




        //void CheckTwitterThreadWork()
        //{
        //    TwitterService ts = new TwitterService();
        //    TwitterSearchResult res =  ts.Se"#radiostart");
        //    OnTweetsTake(res);
        //}
    }
}