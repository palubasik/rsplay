﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Common;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Threading;

namespace RSPlay.Engine
{
    class SoftUpdaterEngine : WebEngineBase
    {
        const string REGEX_UPDATE = "span\\s+id=\\\"version\\\">(\\d{1,})\\.(\\d{1,}).*?<";
        const string VERSION_REGEX = "(\\d{1,})\\.(\\d{1,})";
        public bool IsNewVersion() 
        {
            bool isUpdate = false;
            try
            {
                string resp = RawGet(Constants.UPDATE_URL, string.Empty, true);
                Regex reg = new Regex(REGEX_UPDATE);
                Match mc = reg.Match(resp);
                if (mc.Success)
                {
                    Match mc2 = Regex.Match(Constants.Version, VERSION_REGEX);
                    int maj1 = int.Parse(mc.Groups[1].Value);
                    int maj2 = int.Parse(mc2.Groups[1].Value);
                    int min1 = int.Parse(mc.Groups[2].Value);
                    int min2 = int.Parse(mc2.Groups[2].Value);
                    isUpdate = (maj1 > maj2) || (maj1 == maj2 && min1 > min2);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
            }
            return isUpdate;
        }

        public bool Update() 
        {

            try
            {
                WebClient wc = new WebClient();
                wc.DownloadFile(Constants.UPDATE_FILE_UPDATER_PATH, Constants.UPDATE_FILE_UPDATER_LOCAL_PATH);
                
                System.Diagnostics.Process.Start(Constants.UPDATE_FILE_UPDATER_LOCAL_PATH);
                return true;
            }
            catch (Exception ex) 
            {
                ErrorLogger.Instance.WriteError(ex);
            }

            return false;
        }
    }
}
