using System;
using System.Collections.Generic;
using System.Text;

namespace RSPlay.Engine
{
    static class RssConstants
    {
        public const string Language = "language";
        public const string Link = "link";
        public const string Description = "description";
        public const string Title = "title";
        public const string Guid = "guid";
        public const string PubDate = "pubDate";
        public const string Undefined = "Undefined";
        public const string Rss = "rss";
        public const string Channel = "channel";
        public const string Item = "item";

    }
}
