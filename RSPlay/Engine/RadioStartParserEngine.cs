﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using Common;

namespace RSPlay.Engine
{
    class RadioStartParserEngine : WebEngineBase
    {
        public RadioStartProgramData OnlineProgram { get; set; }

        private List<RadioStartProgramData> programs = new List<RadioStartProgramData>();
        private List<RadioStartNewsData> news = new List<RadioStartNewsData>();

        public List<RadioStartNewsData> News
        {
            get { return news; }
        }

        public List<RadioStartProgramData> Programs
        {
            get { return programs; }
        }

        public void ParsePrograms()
        {
            Programs.Clear();
            OnlineProgram = null;
            string res = RawGet(Constants.RadioStartSite.SERVER_URL, null, true);
            XmlDocument xml = Helper.GetXMLFromHtml(res);
            XmlNodeList xmlnl = xml.GetElementsByTagName("div");
            foreach (XmlNode node in xmlnl) 
            {
                if (node.Attributes["class"] != null && node.Attributes["class"].Value.Contains("program-desc"))
                    ParseProgramNode(node.InnerXml);
            }
        }

        public void ParseNews() 
        {
            RssProcessor rssp = new RssProcessor();
            XmlDocument xml = new XmlDocument();
            string html = RawGet(Constants.RadioStartSite.SERVER_URL_RSS, null, true);
            xml.LoadXml(html);
            rssp.ProcessRss(xml);
            news.Clear();
            foreach (RssNode rssn in rssp.RssDataNodes)
            {
                RadioStartNewsData news1 = new RadioStartNewsData();
                news1.Description = rssn.Description;
                news1.Link = rssn.Link;
                news1.Title = rssn.Title;
                news.Add(news1);
            }
        }
        private void ParseProgramNode(string input)
        {
            string nodexml = HttpUtility.HtmlDecode(input);
            Regex rgImage = new Regex("img\\s+src=\\\"(.*?)\\\"");
            Regex rgMoredesc = new Regex("a\\s+href=\\\"(.*?)\\\"");
            Regex rgDesc = new Regex("<p.*?\\\">(.*?)<");
            Regex rgIsActive = new Regex("color:#d90000");
            Regex rgTitle = new Regex("h3.*?>(.*?)<");
            RadioStartProgramData data = new RadioStartProgramData();
            Match mc = rgImage.Match(nodexml);
            if (mc.Success)
                data.PictureURL = mc.Groups[1].Value;
            mc = rgDesc.Match(nodexml);
            if (mc.Success)
                data.Description = mc.Groups[1].Value;
            mc = rgMoredesc.Match(nodexml);
            if (mc.Success)
                data.MoreDescriptionURL = mc.Groups[1].Value;
            mc = rgIsActive.Match(nodexml);
            if (mc.Success)
            {
                data.IsActive = true;
                string desc2 = RawGet(data.MoreDescriptionURL, null, true);
                XmlDocument xml = Helper.GetXMLFromHtml(desc2);
                XmlNodeList xmlnl = xml.GetElementsByTagName("p");
                foreach (XmlNode node in xmlnl)
                {
                    if (node.HasChildNodes && node.InnerXml.Contains("span id=\"more-"))
                    {
                        data.Description2 = node.InnerText;
                        break;
                    }

                }
                OnlineProgram = data;
            }
            mc = rgTitle.Match(nodexml);
            if (mc.Success)
                data.Title = mc.Groups[1].Value;
            Programs.Add(data);
        }
    }

    public class RadioStartProgramData 
    {
        public string Title;
        public string Description;
        public string Description2;
        public string PictureURL;
        public string MoreDescriptionURL;
        public bool IsActive;
    }

     public class RadioStartNewsData 
    {
        public string Title;
        public string Description;
        public string Link;
    }
}
