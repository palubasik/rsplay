﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using Common;
using System.Drawing;
using System.Compat.Web;
using System.IO;
using System.Threading;

namespace RSPlay.Engine
{
    public class RadioStartChatEngine : WebEngineBase
    {
        //pfc.handleResponse('privmsg2', 'ok', Array('064ee9099d782d6e2ad9477882352a04','test_x'));
        //Here is the command list:
        //ban
        //banlist
        //clear
        //connect
        //debug
        //deop
        //help
        //identify
        //init
        //invite
        //join
        //kick
        //leave
        //me
        //nick
        //op
        //privmsg
        //quit
        //redirect
        //rehash
        //send
        //unban
        //version
        //who
        //whois
        //pfc_ajax=1&f=handleRequest&cmd=%2Fnick%20f8063e9f40ecd94edc9a7902a289c43b%2051825981d320f11d06514d959f51e567%20medvedos
        const string CHAT_NICK_REG = "var\\s+pfc_clientid\\s+=\\s+\\\"([a-zA-Z0-9]+)\\\"";
        const string CHAT_ENTER_POSTDATA_FORMAT = "pfc_ajax=1&f=handleRequest&cmd=%2Fconnect%20{0}%200%20%22{1}%22";
        const string CHAT_SEND_DATA_FORMAT = "pfc_ajax=1&f=handleRequest&cmd=%2Fsend%20{0}%2051825981d320f11d06514d959f51e567%20{1}";
        const string CHAT_UPDATE_FORMAT = "pfc_ajax=1&f=handleRequest&cmd=%2Fupdate%20{0}%2051825981d320f11d06514d959f51e567%20";
        const string CHAT_SEND_COMMAND = "pfc_ajax=1&f=handleRequest&cmd=%2F{0}%20{1}%2051825981d320f11d06514d959f51e567%20{2}";
        const string CHAT_SEND_DATA_FORMAT2 = "pfc_ajax=1&f=handleRequest&cmd=%2Fsend%20{0}%20{1}%20{2}";
        const string CHAT_SEND_PRIVATE_ENTER = "pfc_ajax=1&f=handleRequest&cmd=%2Fprivmsg%20{0}%2051825981d320f11d06514d959f51e567%20%22{1}%22";
        const string STATE_CONNECTED = "handleResponse('nick', 'connected'";
        const string STATE_ISUSED = "handleResponse('nick', 'isused'";
        const string STATE_SOME_ERROR = "pfc.setError";
        const string STATE_FLOOD = "flood_nbmsg\":\"\",\"floodtime";
        const string NEW_MESSAGES_START = "getnewmsg',";
        const string NEW_MESSAGES_END = "]]));";
        //<a\\s+href=\\"(.*?"(.*?)<\\/a>
        const string NEW_USERS = "pfc.handleResponse('who2',";
        const string NEW_USERS_REGEX = "\\\"users\\\":\\{\\\"nick\\\":\\[(.*?)\\]";
      //  "nick":"sc"
        const string JSON_TEMPLATE = "{0}\\\":\\\"(.*?)\\\"";
        const string JSON_ARRAY_TEMPLATE = "{0}\\\":\\[(.*?)\\]";
        const string CHAN_ID = "51825981d320f11d06514d959f51e567";
        const string NEW_USER_REGEX = "nick\\\":\\\"(.*?)\\\"";
        const string NEW_MESSAGE_DATA_REGEX = "\\[(.*?)\\]";
        const string JSON_MESSAGE_SPLIT_REGEX = ",\"(.*?)\"";
        const string STATE_NOT_CONNECTED = "You must be connected to send a message";
        const string JSON_ARRAY_ITEM_VALUES = "Array\\((.*?)\\)";
        string sessionID;
        string cookies;
        string nick;

        public string Nick
        {
            get { return nick; }
        }

        static object chatUpdateLocker = new object();
        //make private + constructor
        private List<ChatMessageData> messages = new List<ChatMessageData>();
        private List<PrivateChannelData> privateChannels = new List<PrivateChannelData>();

        public List<ChatMessageData> Messages
        {
            get { return messages; }
        }

        private List<string> users = new List<string>();

        public List<string> Users
        {
            get { return users; }
        }


        public RadioStartChatEngine()
        {

        }

        public ChatState Enter(string name) 
        {
            ChatState res = ChatState.SOME_ERROR;
            try{
                GetNickID();
                string postDataStr = string.Format(CHAT_ENTER_POSTDATA_FORMAT, sessionID , name);

                string resp = RawPost(Constants.RadioStartSite.SERVER_CHAT_URL, postDataStr, null, true);
                if(!resp.Contains(STATE_CONNECTED))
                {
                    res = ChatState.NEED_CONNECTED;
                    if(resp.Contains(STATE_ISUSED))
                        res = ChatState.IS_USED;
                    return res;
                }

                cookies = LastResponse.Headers["Set-Cookie"];
                ParseChatItems(resp);
                nick = name;
                res = ChatState.OK;
            }
            catch(Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
            }
           
            return res;
        }
        
        private List<string> GetJsonData(string nodename, string json, bool hex) 
        {
            List<string> res = new List<string>();

            Regex jsonSingle = new Regex(string.Format(JSON_TEMPLATE, nodename));
            foreach (Match mc in jsonSingle.Matches(json)){
                string data = mc.Groups[1].Value;
                if (hex)
                    res.Add(FromHexStringToString(data));
                else
                    res.Add(data);
            }
            Regex jsonMulti = new Regex(string.Format(JSON_ARRAY_TEMPLATE, nodename));
            foreach (Match mc in jsonMulti.Matches(json))
            {
                string data = mc.Groups[1].Value;
                if (hex)
                    res.Add(FromHexStringToString(data));
                else
                    res.Add(data);
            }
            return res;
        }
        private ChatState ParseChatItems(string resp)
        {
            ChatState cs = ChatState.NO_CHANGES;

            lock (chatUpdateLocker)
            {
                Messages.Clear();
                string[] commands = resp.Split(new string[] { "pfc.handleResponse('" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string command in commands)
                {
                    string command_name = command.Substring(0, command.IndexOf('\''));
                    switch (command_name)
                    {
                        case "update":
                            continue;
                        case "who":
                        case "who2":
                            ParseUsers(command);
                            cs = ChatState.OK;
                            break;
                        case "privmsg":
                        case "privmsg2":
                            PrivateChannelParseOperations(command);
                            cs = ChatState.OK;
                            break;
                        case "getnewmsg":
                            ParseMessages(command);
                            cs = ChatState.OK;
                            break;
                    }
                }
            }

            return cs;
        }

       
        private void ParseMessages(string resp)
        {
            int messageStart = resp.IndexOf(NEW_MESSAGES_START);
            if (messageStart >= 0)
            {
                messageStart += NEW_MESSAGES_START.Length - 2;
                string messages = resp.Substring(messageStart);
                int messageEnd = messages.IndexOf(NEW_MESSAGES_END);
                messages = messages.Substring(0, messageEnd + NEW_MESSAGES_END.Length);


                Regex messageReg = new Regex(NEW_MESSAGE_DATA_REGEX);
                MatchCollection matches = messageReg.Matches(messages);
                this.messages.Clear();
                foreach (Match m in matches)
                {
                    string group_matches = m.Groups[1].Value;
                    Match replaceHref = Regex.Match(group_matches, "<a\\s+href=\\\\\"(.*?)\\\\\".*?a>");
                    if (replaceHref.Success)
                    {
                        group_matches = group_matches.Replace(replaceHref.Groups[0].Value, replaceHref.Groups[1].Value);
                    }
                    group_matches = group_matches.Replace("\\/", "/").Replace("\\\"","%22");
                    Regex messageReg2 = new Regex(JSON_MESSAGE_SPLIT_REGEX);
                    MatchCollection mcc = messageReg2.Matches(group_matches);
                    string unformed_msg = mcc[5].Groups[1].Value;
                    bool isPrivate = false;
                    string channel = mcc[3].Groups[1].Value;
                    string user = FromHexStringToString(mcc[2].Groups[1].Value);
                    bool isMyMessage = user == nick;
                    PrivateChannelData pc = privateChannels.Find(x => x.Channel == channel);
                    if (pc != null)
                    {
                        isPrivate = true;
                        if (user == nick)
                            user = pc.User;
                    }

                    string mess = string.Empty;
                    mess = FromHexStringToString(unformed_msg);
                    this.messages.Add(new ChatMessageData()
                    {
                        DateTimeStr = string.Format("{0} {1}", mcc[0].Groups[1].Value, mcc[1].Groups[1].Value),
                        User = user,
                        Message = mess,
                        IsPrivate = isPrivate,
                        MyMessage = isMyMessage
                    });
                }
            }
        }

        private void ParseUsers(string resp)
        {
            if (!resp.Contains(CHAN_ID))
                return;
            Regex usersReg = new Regex(NEW_USERS_REGEX);
            Match mc = usersReg.Match(resp);
            if (mc.Success)
            {
                users.Clear();
                string[] items = mc.Groups[1].Value.Split(',');
                foreach (string user in items)
                {
                    users.Add(FromHexStringToString(user.Replace("\"", string.Empty)));
                }
            }
            //next portion nick:[
            Regex newuserReg = new Regex(NEW_USER_REGEX);
            foreach (Match m in usersReg.Matches(resp))
            {
                if (mc.Success)
                {
                    string[] items = mc.Groups[1].Value.Split(',');
                    foreach (string item in items)
                    {
                        string user = item;
                        user = FromHexStringToString(user.Replace("\"", string.Empty));
                        if (!users.Contains(user))
                            users.Add(user);
                    }
                }
            }
        }

        private PrivateChannelData PrivateChannelParseOperations(string resp)
        {
            PrivateChannelData pc = null;
            Regex checkPrivate = new Regex(JSON_ARRAY_ITEM_VALUES);
            Match cpm = checkPrivate.Match(resp);
            if (cpm.Success)
            {
                string[] priv_chan = cpm.Groups[1].Value.Split(',');
                string priv_user = priv_chan[1].Replace("'", "").Trim();
                string chan = priv_chan[0].Replace("'", "").Trim();
                pc = privateChannels.Find(x => x.User == priv_user);
                if (pc != null)
                    privateChannels.Remove(pc);
                pc = new PrivateChannelData() { User = priv_user, Channel = chan };
                privateChannels.Add(pc);
            }
            return pc;
        }

        static string FromHexStringToString(string mess)
        {
            Regex rx = new Regex(@"\\[uU]([0-9a-fA-F]{4})");
            string res = rx.Replace(mess,
                match => char.ConvertFromUtf32(int.Parse(match.Groups[1].Value, NumberStyles.HexNumber)));
            return HttpUtility.HtmlDecode(res.Replace("%22","\""));
        }
    
        public ChatState ChatUpdate()
        {
            ChatState res = ChatState.SOME_ERROR;
            try
            {
                string postDataStr = string.Format(CHAT_UPDATE_FORMAT, sessionID, nick);
                string resp = ChatRawRequest(postDataStr);

                res = ParseChatItems(resp);
                //if (res != ChatState.NO_CHANGES)
                //{
                //    File.AppendAllText("responses.txt", resp);
                //}

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
            }
            return res;
        }

        private void GetNickID()
        {
            string data = base.RawGet(Constants.RadioStartSite.SERVER_CHAT_URL, null, false);

            Regex rg = new Regex(CHAT_NICK_REG);
            Match mc = rg.Match(data);
            if (mc.Success)
            {
                sessionID = mc.Groups[1].Value;
            }
        }

        private string ChatRawRequest(string data)
        {
            return RawPost(Constants.RadioStartSite.SERVER_CHAT_URL, data, cookies, true);
        }

        public ChatState SendCommand(string command,string data) 
        {
            ChatState res = ChatState.SOME_ERROR;
            try
            {
                data = Helper.HTMLURLEncode(data);
                string postDataStr = string.Format(CHAT_SEND_COMMAND,command, sessionID, data);
                string resp = RawPost(Constants.RadioStartSite.SERVER_CHAT_URL, postDataStr, cookies, true);
                if (resp.Contains(STATE_NOT_CONNECTED) || resp.Contains(STATE_ISUSED) || resp.Contains(STATE_SOME_ERROR))
                    res = ChatState.NEED_CONNECTED;
                else
                {
                    ParseChatItems(resp);
                    //   Messages.Add(string.Format("[{0} {1}] {2}: {3}", mcc[1].Groups[1].Value.Replace("\\", ""), mcc[2].Groups[1].Value, FromHexStringToString(mcc[3].Groups[1].Value), mess));
                    res = ChatState.OK;
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
            }
            return res;
        }
        

        public ChatState SendMessage(string message) 
        {
            ChatState res = ChatState.SOME_ERROR;
            try
            {

                message = Helper.HTMLURLEncode(message);
                string postDataStr = string.Format(CHAT_SEND_DATA_FORMAT, sessionID, message);
                string resp = RawPost(Constants.RadioStartSite.SERVER_CHAT_URL, postDataStr, cookies, true);
                if (resp.Contains(STATE_NOT_CONNECTED) || resp.Contains(STATE_ISUSED) || resp.Contains(STATE_SOME_ERROR))
                {
                    res = ChatState.NEED_CONNECTED;
                    ErrorLogger.Instance.WriteError(new Exception(), resp);
                }
                else
                {
                    ParseChatItems(resp);
                    //   Messages.Add(string.Format("[{0} {1}] {2}: {3}", mcc[1].Groups[1].Value.Replace("\\", ""), mcc[2].Groups[1].Value, FromHexStringToString(mcc[3].Groups[1].Value), mess));
                    res = ChatState.OK;
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) 
            {
                ErrorLogger.Instance.WriteError(ex);
            }
            return res;
        }

        public ChatState SendPrivateMessage(string to, string message) 
        {
            ChatState res = ChatState.SOME_ERROR;
            try
            {
                PrivateChannelData pc = privateChannels.Find(x => x.User == to);
                if (pc == null)
                {
                    string postDataStr = string.Format(CHAT_SEND_PRIVATE_ENTER, sessionID, to);
                    string resp = RawPost(Constants.RadioStartSite.SERVER_CHAT_URL, postDataStr, cookies, true);
                    pc = PrivateChannelParseOperations(resp);
                    if (pc == null)
                        return ChatState.SOME_ERROR;
                }
                message = Helper.HTMLURLEncode(message);
                string post = string.Format(CHAT_SEND_DATA_FORMAT2, sessionID, pc.Channel, message);
                string resp2 = RawPost(Constants.RadioStartSite.SERVER_CHAT_URL, post, cookies, true);
                if (resp2.Contains(STATE_NOT_CONNECTED) || resp2.Contains(STATE_ISUSED) || resp2.Contains(STATE_SOME_ERROR))
                {
                    res = ChatState.NEED_CONNECTED;
                }
                else
                {
                    ParseChatItems(resp2);
                    //   Messages.Add(string.Format("[{0} {1}] {2}: {3}", mcc[1].Groups[1].Value.Replace("\\", ""), mcc[2].Groups[1].Value, FromHexStringToString(mcc[3].Groups[1].Value), mess));
                    res = ChatState.OK;
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception e) 
            {
                ErrorLogger.Instance.WriteError(e);
            }
            return res;
        }
    }

    public class ChatMessageData 
    {
        public string DateTimeStr;
        public string Message;
        public string User;
        public bool IsPrivate = false;
        public bool MyMessage = false;
    }
    public class PrivateChannelData 
    {
        public string User;
        public string Channel;
    }
    public enum ChatState 
    {
        OK,
        NEED_CONNECTED,
        IS_USED,
        SOME_ERROR,
        FLOOD,
        NO_CHANGES
    }
}
