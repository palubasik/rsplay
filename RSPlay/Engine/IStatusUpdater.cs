﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSPlay.Engine
{
    public delegate void UpdateStatus(string message);

    public interface IStatusUpdater
    {
        event UpdateStatus OnStatusUpdate; 
    }
}
