﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RSPlay.Engine
{
    public delegate PulseState PulseThreadFunc(object data);

    class PulseThread
    {
        Thread thread = null;
        PulseThreadFunc func = null;
        bool worked = false;

        public bool Worked
        {
            get { return worked; }
        }
        int interval;

        object state_data;

        public void PushStateData(object data) 
        {
            state_data = data;
        }
        public object PopStateData() 
        {
            return state_data;
        }

        public PulseThread(PulseThreadFunc threadFunc, int interval) 
        {
            func = threadFunc;
            this.interval = interval;
        }

        public void Start() 
        {
            Start(string.Empty);
        }

        public void Start(object data) 
        {
            Stop();
            thread = new Thread(new ParameterizedThreadStart(ThreadFunc));
            worked = true;
            thread.Start(data);
        }

        private void ThreadFunc(object data)
        {
            int i = interval * 10;
            while (worked)
            {
                if ((++i) >= interval * 10)
                {
                    i = 0;
                    if (func(data) == PulseState.ABORT)
                    {
                        worked = false;
                        continue;
                    }
                }
                Thread.Sleep(100);
            }
        }
                    
        public void Stop(int wait = -1) 
        {
            if (thread != null) 
            {
                worked = false;
                thread.Abort();
                if (wait > 0)
                    thread.Join(wait);
                else
                    thread.Join();
                thread = null;
            }
        }
    }

    public enum PulseState
    {
        ABORT,
        CONTINUE
    }
}
