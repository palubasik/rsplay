﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using Common;

namespace RSPlay
{
    public partial class ErrorForm : Form
    {
        public string ErrorInformation
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public ErrorForm()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                MailSender.Instance.Subject = string.Format("Error happens on {0}",SettingInfo.Instance.SettingData.MachineIdentifier);
                MailSender.Instance.Message = String.Format("{0}{1}{2}{1}", ErrorInformation, Environment.NewLine, SettingInfo.Instance.ToString());
                MailSender.Instance.AddAttachments(new string[] { ErrorLogger.Instance.LogFile });
                MailSender.Instance.SendMailLocal();
                MessageBox.Show("Ваше сообщение отправлено!", "Обратная связь", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                ErrorLogger.Instance.WriteError(ex);
                MessageBox.Show("Ошибка при отправки сообщения((", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            textBox1.Visible = !textBox1.Visible;

            if (textBox1.Visible)
            {
                this.Height = 354;
            }
            else
                this.Height = 174;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.No;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Yes;
            Close();
        }

        private void ErrorForm_Load(object sender, EventArgs e)
        {
            linkLabel1_LinkClicked(null, null);
        }
    }
}
