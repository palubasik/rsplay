﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RSPlay
{
    public partial class ScrobbleParameterForm : Form
    {
        public int ScrobbleValue {
            get { return (int.Parse(comboBox1.SelectedItem.ToString()) / 5) - 1; }
            set { comboBox1.SelectedItem = (value + 1)* 5; }
        }

        public ScrobbleParameterForm()
        {
            InitializeComponent();
            for (int i = 1; i < 20; i++)
            {
                comboBox1.Items.Add(i * 5);
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void PinForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1_Click(null, null);
        }

        private void PinForm_Load(object sender, EventArgs e)
        {
            button1.KeyDown += new KeyEventHandler(PinForm_KeyDown);
           
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            PinForm_KeyDown(sender, e);
        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            PinForm_KeyDown(sender, e);
        }
    }
}
