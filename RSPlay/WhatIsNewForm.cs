﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Common;
using RSPlay.Properties;

namespace RSPlay
{
    public partial class WhatIsNewForm : Form
    {
        public WhatIsNewForm()
        {
            InitializeComponent();
        }

        private void ChatHistoryForm_Load(object sender, EventArgs e)
        {
           
        }

        private void WhatIsNewForm_Load(object sender, EventArgs e)
        {
            richTextBoxEx1.AppendRtf(Resources.WhatIsNew);
           richTextBoxEx1.ScrollToBegin();
        }
    }
}
