﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using Common;

namespace RSPlay.UserControls
{
    public partial class NewsDisplayUserControl : UserControl
    {
        string moreUrl;
        public NewsDisplayUserControl(RadioStartNewsData data)
        {
            InitializeComponent();
            this.labelDetails.Text = data.Description.Replace("[...]","");
            this.labelCaption.Text = data.Title;
            this.linkLabelMore.Click += new EventHandler(linkLabelMore_Click);
            moreUrl = data.Link;
        }

        void linkLabelMore_Click(object sender, EventArgs e)
        {
            Helper.OpenLink(moreUrl);
        }

        private void textBoxDetails_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }
    }
}
