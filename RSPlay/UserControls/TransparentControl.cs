﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace RSPlay.UserControls
{
    public partial class TransparentControl : UserControl
    {
        
        private Image image;
        private int frameCount = 0;
        FrameDimension dimension = null;
        private int index = -1;
        public TransparentControl()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.Transparent;
            ForeColor = Color.Transparent;
            refresher = new Timer();
            refresher.Tick += TimerOnTick;
            refresher.Interval = 50;
            refresher.Enabled = true;
            refresher.Start();

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x20;
                return cp;
            }
        }

        protected override void OnMove(EventArgs e)
        {
            RecreateHandle();
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            if (image != null)
            {
                if (++index > frameCount - 1)
                    index = 0;
                 image.SelectActiveFrame(dimension, index);
                e.Graphics.DrawImage(image, (Width / 2) - (image.Width / 2), (Height / 2) - (image.Height / 2));
              
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //Do not paint background
           // base.OnPaintBackground(e);
        }

        //Hack
        public void Redraw()
        {
            RecreateHandle();
            
        }

        private void TimerOnTick(object source, EventArgs e)
        {
            RecreateHandle();
            refresher.Stop();
        }

        public Image Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                RecreateHandle();
                dimension = new FrameDimension(image.FrameDimensionsList[0]);
                frameCount = image.GetFrameCount(dimension);

            }
        }

      
      

    }
}
