﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Drawing.Drawing2D;

namespace RSPlay.UserControls
{
    public class ChatDisplayUserControl : System.Windows.Forms.UserControl
    {
        private System.Windows.Forms.VScrollBar vScrollHTML;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.IContainer components;

        public static Hashtable EmoticonMap = new Hashtable(); // table for graphic guys

        public ChatDisplayUserControl()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            InitializeEmoticonMap();

            // double buffering
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
            ResizeScroll();
            // TODO: Add any initialization after the InitializeComponent call
            Reset();
        }

        public ImageList EmoticonImages
        {
            get
            {
                return imageList1;
            }

            set
            {
                imageList1 = value;
            }
        }

        void InitializeEmoticonMap()
        {
            EmoticonMap[":)"] = 1;
            EmoticonMap[":-)"] = 1;
            EmoticonMap[":-("] = 2;
            EmoticonMap[":("] = 2;
            EmoticonMap["8-)"] = 3;
            EmoticonMap[":-S"] = 4;
            EmoticonMap[":-@"] = 5;
            EmoticonMap["+o("] = 6;
            EmoticonMap[":-|"] = 7;
            EmoticonMap[";-)"] = 8;
            EmoticonMap[":-#"] = 9;
            EmoticonMap["(A)"] = 10;
            EmoticonMap["(6)"] = 11;
            EmoticonMap["(%)"] = 12;
            EmoticonMap["(N)"] = 13;
            EmoticonMap["(Y)"] = 14;
            EmoticonMap["(P)"] = 15;
            EmoticonMap[":-$"] = 16;
            EmoticonMap["<:o)"] = 15;
        }

        void Reset()
        {
            _position = 0;
            _verticalPosition = 0;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ChatDisplayUserControl));
            this.vScrollHTML = new System.Windows.Forms.VScrollBar();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // vScrollHTML
            // 
            this.vScrollHTML.Location = new System.Drawing.Point(328, 0);
            this.vScrollHTML.Name = "vScrollHTML";
            this.vScrollHTML.Size = new System.Drawing.Size(17, 272);
            this.vScrollHTML.TabIndex = 1;
            this.vScrollHTML.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollHTML_Scroll);
            // 
            // imageList1
            // 
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // HTMLDisplay
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.vScrollHTML);
            this.Name = "HTMLDisplay";
            this.Size = new System.Drawing.Size(352, 280);
            this.Resize += new System.EventHandler(this.HTMLDisplay_Resize);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.HTMLDisplay_Paint);
            this.ResumeLayout(false);

        }
        #endregion

        int _position = 0;
        int _verticalPosition = 0;

        /// <summary>
        /// Draws the Chat Text with proper coloring and font
        /// based on the html tags and emoticons inside the text
        /// </summary>
        /// <param name="g"></param>
        private void Draw(Graphics g)
        {
            Color color = Color.Black;
            Font font = null;
            Brush b = null;
            bool nextLine = false;
            bool last = false;
            bool hasTab = false;
            int smileyGraphic = 0;

            try
            {
                // we want really smooth drawing for our chat
                // speed of drawing isn't an issue here
                g.SmoothingMode = SmoothingMode.AntiAlias;

                // create the parser with the user control font
                // and the current text of our HTMLDisplay
                _parser = new HtmlParser(_html, Font);



                // start the parser from the beginning
                Reset();
                _parser.Reset();
                _scrollPosition = vScrollHTML.Value;

                // keep processing the html text until we have no more
                while (last == false)
                {
                    // get the next piece of relevant text from the state machine
                    // along with all its relative font, color, and position information
                    string text = _parser.ProcessNext(ref font, ref color, ref nextLine, ref last, ref hasTab, ref smileyGraphic);

                    // create a brush to color the text
                    b = new SolidBrush(color);

                    int stringWidth = (int)g.MeasureString(text, font).Width;
                    int stringHeight = (int)g.MeasureString("X", font).Height + 2;

                    if (_position + stringWidth > ClientRectangle.Width - vScrollHTML.Width)
                    {
                        _position = _tabWidth;
                        _verticalPosition += stringHeight;
                    }

                    // get clip region for smart validation
                    Rectangle clip = Rectangle.Truncate(g.ClipBounds);

                    // calculate the absolute vertical position in client coordinates
                    // by taking the scrollbar into account
                    int absoluteVerticalPosition = _verticalPosition - _scrollPosition;

                    // if its not an emoticon, we need to draw a string
                    if (smileyGraphic == 0)
                    {
                        if (_position <= clip.Right && absoluteVerticalPosition <= clip.Bottom && absoluteVerticalPosition >= clip.Top)
                        {
                            // draw the string here with the determined font, color and position
                            g.DrawString(text, font, b, _position, absoluteVerticalPosition, new StringFormat());
                        }
                    }


                    // track the current horizontal position
                    _position += (int)stringWidth;

                    // if the state machine determine we need to go to the next line,
                    // recalculate current horizontal and vertical position
                    if (nextLine)
                    {
                        _position = 0;
                        _verticalPosition += stringHeight;
                    }

                    // if the state machine determined we have a tab at the next token,
                    // calculate the position of the tab
                    if (hasTab)
                    {
                        _position = _tabWidth;
                    }

                    // handle the case where we don't have text, but an emoticon graphic
                    if (smileyGraphic > 0)
                    {
                        if (_position <= clip.Right && absoluteVerticalPosition <= clip.Bottom && absoluteVerticalPosition >= clip.Top)
                        {
                            // draw the emoticon bitmap image from the ImageList
                            g.DrawImage(imageList1.Images[smileyGraphic - 1], _position, _verticalPosition - _scrollPosition, imageList1.Images[0].Width, imageList1.Images[0].Height);
                        }
                        _position += imageList1.Images[smileyGraphic - 1].Width + 2;
                    }

                    b.Dispose();  // clean up the brush resources
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            // 

        }

        private int _tabWidth = 100;
        public int TabWidth
        {
            get
            {
                return _tabWidth;
            }

            set
            {
                _tabWidth = value;
            }
        }

        private HtmlParser _parser = null;

        private void HTMLDisplay_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // draw the control based on html
            Draw(e.Graphics);

        }

        private string _html = "";

        public string Html
        {
            get
            {
                return _html;
            }

            set
            {
                _html = value;
                ComputeScrollPosition();
                Invalidate();
            }

        }

        int _scrollPosition = 0;
        void ComputeScrollPosition()
        {
            vScrollHTML.Minimum = 0;
            vScrollHTML.Maximum = _verticalPosition - ClientRectangle.Height / 2;
            if (vScrollHTML.Maximum < 0)
                vScrollHTML.Maximum = 0;
            vScrollHTML.Value = vScrollHTML.Maximum;
            vScrollHTML.SmallChange = this.Font.Height;
            vScrollHTML.LargeChange = vScrollHTML.Maximum / 10;
        }

        private void vScrollHTML_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            Invalidate();
        }

        void ResizeScroll()
        {
            vScrollHTML.SetBounds(ClientRectangle.Width - vScrollHTML.Width, 0, vScrollHTML.Width, ClientRectangle.Height);
        }

        private void HTMLDisplay_Resize(object sender, System.EventArgs e)
        {
            ResizeScroll();
            ComputeScrollPosition();
            Invalidate();
        }
    }
}
