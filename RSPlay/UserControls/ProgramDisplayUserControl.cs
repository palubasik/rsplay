﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using Common;

namespace RSPlay.UserControls
{
    public partial class ProgramDisplayUserControl : UserControl
    {
        string moreUrl;
        public ProgramDisplayUserControl(RadioStartProgramData data)
        {
            InitializeComponent();
            this.pictureBox.LoadAsync(data.PictureURL);
            this.labelDetails.Text = data.Description;
            if (data.IsActive)
                this.labelCaption.ForeColor = Color.Red;
            this.labelCaption.Text = data.Title;
            this.linkLabelMore.Click += new EventHandler(linkLabelMore_Click);
            moreUrl = data.MoreDescriptionURL;
        }

        void linkLabelMore_Click(object sender, EventArgs e)
        {
            Helper.OpenLink(moreUrl);
        }

        private void textBoxDetails_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }
    }
}
