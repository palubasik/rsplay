﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace RSPlay.UserControls
{
    public class HtmlParser
    {
        const int TAB_POSITION = 50;

        string _currentTag;
        string _currentEndTag;
        string _currentText;
        string _html;
        int _ptr = 0;

        Font _boldFont;
        Font _italicFont;
        Font _underlineFont;
        Font _largeFont;
        Font _regularFont;
        Font _smileyFont;

        Color _currentColor = Color.Black;
        Font _currentFont;

        public HtmlParser(string html, Font currentFont)
        {
            _html = html;

            InitializeFonts(currentFont);
        }

        void InitializeFonts(Font currentFont)
        {
            _boldFont = new Font(currentFont.FontFamily.Name, currentFont.Size, FontStyle.Bold);
            _italicFont = new Font(currentFont.FontFamily.Name, currentFont.Size, FontStyle.Italic);
            _underlineFont = new Font(currentFont.FontFamily.Name, currentFont.Size, FontStyle.Underline);
            _largeFont = new Font(currentFont.FontFamily.Name, currentFont.Size, FontStyle.Regular);
            _regularFont = new Font(currentFont.FontFamily.Name, currentFont.Size, FontStyle.Regular);
            _smileyFont = new Font("WingDings", currentFont.Size + 5, FontStyle.Regular);

            _currentFont = _regularFont;
        }

        public enum State
        {
            Idle,
            MayHaveTag,
            MayHaveEndTag,
            HasTag,
            HasEndTag,
            HasTab,
            HasReturn,
            HasSpace,
            HasSmiley,
            CaptureText
        }

        State _state = State.Idle;

        public State HtmlState
        {
            get
            {
                return _state;
            }
        }

        int _startTag;
        int _endTag;
        int _startEndTag;
        int _endEndTag;
        int _lastPtr = 0;
        string _currentEmoticon = "";

        bool _finished = false;

        string _tmpText;

        public void Reset()
        {
            _ptr = 0;
            _lastPtr = 0;
            _startTag = 0;
            _endTag = 0;
            _startEndTag = 0;
            _endEndTag = 0;
            _state = State.Idle;
            _finished = false;
        }

        private string FindEmoticon()
        {
            foreach (string face in ChatDisplayUserControl.EmoticonMap.Keys)
            {
                // see if we found an emoticon
                if (_html.Length - _ptr >= face.Length)  // has to be as least as big as face
                {
                    if (_html.Substring(_ptr, face.Length) == face)
                    {
                        return face;
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// Get the next phrase
        /// </summary>
        /// <returns></returns>
        string GetNextPhrase()
        {
            while (_ptr < _html.Length || _state == State.HasEndTag)
            {
                switch (_state)
                {
                    case State.Idle:
                        _finished = false;
                        string emoticonValue = FindEmoticon();
                        if (_html[_ptr] == '<')
                        {
                            // may have found a tag

                            if (_html.Length > _ptr + 1 && _html[_ptr + 1] == '/')
                            {
                                // mistake
                            }
                            else
                            {
                                _startTag = _ptr;
                                _state = State.MayHaveTag;
                                // capture all text before
                                _currentText = _html.Substring(_lastPtr, _ptr - _lastPtr);
                                return _currentText;
                            }
                        }
                        else if (_html[_ptr] == '\t')
                        {
                            _state = State.HasTab;
                        }
                        else if (_html[_ptr] == '\n')
                        {
                            _state = State.HasReturn;
                        }
                        else if (_html[_ptr] == ' ')
                        {
                            _state = State.HasSpace;
                        }
                        else if (emoticonValue.Length > 0)
                        {
                            _currentEmoticon = emoticonValue;
                            _state = State.HasSmiley;
                        }
                        else
                        {
                            // increase position
                            // also get text up to this point
                            _tmpText = "";
                            _tmpText = _html.Substring(_lastPtr, (_ptr - _lastPtr));
                        }

                        break;
                    case State.MayHaveTag:
                        if (_html[_ptr] == '>')
                        {
                            _endTag = _ptr;
                            _currentTag = _html.Substring(_startTag, (_endTag - _startTag) + 1);
                            _state = State.HasTag;
                        }
                        break;
                    case State.HasTag:
                        // get the current text
                        _lastPtr = _ptr;
                        _currentText = _tmpText;
                        _state = State.CaptureText;
                        break;
                    case State.CaptureText:
                        if (_html.Length > _ptr + 1 && _html[_ptr] == '<' && _html[_ptr + 1] == '/')
                        {
                            // found end tag
                            _startEndTag = _ptr;
                            // get inner text
                            _currentText = _html.Substring(_endTag + 1, _startEndTag - (_endTag + 1));
                            _state = State.MayHaveEndTag;
                        }
                        break;
                    case State.MayHaveEndTag:
                        if (_html[_ptr] == '>')
                        {
                            _endEndTag = _ptr;
                            _currentEndTag = _html.Substring(_startEndTag, (_endEndTag - _startEndTag) + 1);
                            _state = State.HasEndTag;
                        }
                        break;
                    case State.HasEndTag:
                        // get the current text
                        _lastPtr = _ptr;
                        _state = State.Idle;
                        return _currentTag;
                    case State.HasTab:
                        _currentText = _html.Substring(_lastPtr, (_ptr - _lastPtr));
                        _lastPtr = _ptr;
                        _state = State.Idle;
                        return "\t";
                    case State.HasSpace:
                        _currentText = _html.Substring(_lastPtr, (_ptr - _lastPtr));
                        _lastPtr = _ptr;
                        _state = State.Idle;
                        return " ";
                    case State.HasSmiley:
                        _currentText = _html.Substring(_lastPtr, (_ptr - _lastPtr));
                        _ptr += _currentEmoticon.Length - 1;
                        _lastPtr = _ptr;
                        _state = State.Idle;
                        return "<g>";
                    case State.HasReturn:
                        _currentText = _html.Substring(_lastPtr, (_ptr - _lastPtr) - 1);
                        _lastPtr = _ptr;
                        _state = State.Idle;
                        return "\n";
                    default:
                        break;
                }  // end switch parser

                _ptr++;
            } // end while

            // finished while, return phrase
            _currentText = _html.Substring(_lastPtr, _html.Length - _lastPtr);
            _finished = true;
            return _currentText;
        }

        const string COLOR_TOKEN = "color=";
        public Color ExtractColor(string token)
        {
            // contains color if
            try
            {
                int tokenPosition = token.IndexOf(COLOR_TOKEN);
                if (tokenPosition >= 0)
                {
                    tokenPosition += COLOR_TOKEN.Length; // move to the end
                    int endTag = token.IndexOf(">");
                    // extract color name
                    string colorName = token.Substring(tokenPosition, endTag - tokenPosition);
                    Color aColor = Color.FromName(colorName);
                    return aColor;
                }
            }
            catch
            {
            }

            return Color.Black;
        }

        public string ProcessNext(ref Font f, ref Color c, ref bool nextLine, ref bool last, ref bool tab, ref int smileyGraphic)
        {
            string nextToken = GetNextPhrase();

            smileyGraphic = 0;
            nextLine = false;
            tab = false;

            string partialToken = nextToken;
            if (nextToken.Length > 2)
            {
                partialToken = nextToken.Substring(0, 2);
            }

            switch (partialToken)
            {
                case "<b":
                    _currentFont = _boldFont;
                    break;
                case "<u":
                    _currentFont = _underlineFont;
                    break;
                case "<i":
                    _currentFont = _italicFont;
                    break;
                case "<l":
                    _currentFont = _largeFont;
                    break;
                case "<g":
                    smileyGraphic = (int)ChatDisplayUserControl.EmoticonMap[_currentEmoticon];
                    break;
                case "\n":
                    nextLine = true;
                    break;
                case "\t":
                    tab = true;
                    break;
                case " ":
                    break;
                default:
                    _currentFont = _regularFont;
                    break;
            }

            _currentColor = ExtractColor(nextToken);
            c = _currentColor;
            last = _finished;
            f = _currentFont;
            _currentFont = _regularFont;

            return _currentText;
        }

    }
}
