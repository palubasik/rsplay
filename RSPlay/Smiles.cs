﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;
using RSPlay.Properties;
using System.Drawing.Drawing2D;

namespace RSPlay
{
    static class Smiles 
    {
        static Dictionary<string, Image> smiles = new Dictionary<string, Image>();
        static string[] smiles_text = null; 
        //readonly string[] smiles = new string[] { ":)", ":(", ":|", "0_0" };//LATER
         static Smiles()
        {
            // emoticon_smile.png	 :-) ^_^ :)
            //emoticon_evilgrin.png	 >(
            //emoticon_surprised.png   :S :s :-S :-s :-/
            //emoticon_grin.png        :-D :D
            //emoticon_unhappy.png     :'( :-( :o( :-< :(
            //emoticon_happy.png       :lol:
            //emoticon_waii.png	 :{} :-{} :razz: :} :-}
            //emoticon_wink.png	 ;-) ;o) ;)
            //emoticon_tongue.png      :P :-P :-p :p
            //weather_rain.png         /// \\\ ||| :rain: :drizzle:
            //weather_snow.png         :***:
            //weather_sun.png          >O<
            //weather_clouds.png       :""": :cloud: :clouds: 
            //weather_cloudy.png       :"O": :cloudly:
            //weather_lightning.png    :$:
            //arrow_right.png		 => -> --> ==> >>>
            //arrow_left.png		 <= <- <-- <== <<<
            //exclamation.png		 :!:		
            //lightbulb.png		 *) 0=

            smiles[":)"] = Resources.emoticon_smile;
            smiles[";-)"] = Resources.emoticon_wink;
            smiles[";)"] = Resources.emoticon_wink;
            smiles["^_^"] = Resources.emoticon_smile;
            smiles[":-)"] = Resources.emoticon_smile;
            smiles[":("] = Resources.emoticon_unhappy;
            smiles[":'("] = Resources.emoticon_unhappy;
            smiles[":-("] = Resources.emoticon_unhappy;
            smiles[":D"] = Resources.emoticon_grin;
            smiles[":-D"] = Resources.emoticon_grin;
            smiles[":s"] = Resources.emoticon_surprised;
            smiles[":S"] = Resources.emoticon_surprised;
            smiles["0_0"] = Resources.emoticon_surprised;
            smiles[":lol:"] = Resources.emoticon_happy;
            smiles["*love*"] = ResizeImage(Resources.red_heart,32,32);
            smiles[":p"] = Resources.emoticon_tongue;
            smiles["twitter"] = Resources.twitter;
            smiles["done"] = Resources.done;
            smiles[":!:"] = Resources.exclamation;
            smiles["radioSTART"] = ResizeImage(Resources.logo, 32, 12);
            smiles["*radio*"] = ResizeImage(Resources.logo,32,12);
            smiles["бчб"] = ResizeImage(Resources._56439927,16,16);
            smiles["bender"] = Resources._220px_Bender_Rodriguez;
            smiles["с блекджеком и шлюхами"] = Resources._220px_Bender_Rodriguez;
          
            //smiles["$_$"] = ResizeImage(Resources.money,32,32);
            //smiles[":|"] = ResizeImage(Resources.nothing,32,32);
            //smiles["*radio*"] = ResizeImage(Resources.logo,32,32);
            //smiles["*yeah*"] = ResizeImage(Resources.victory,32,32);
            //smiles[":|"] = ResizeImage(Resources.nothing,32,32);
            //smiles[":p"] = ResizeImage(Resources.grimace,32,32);
            //smiles[":P"] = ResizeImage(Resources.anger,32,32);
            smiles_text = new string[smiles.Keys.Count];
            int idx = -1;
            foreach (string key in smiles.Keys)
                smiles_text[++idx] = key;
        }

         static Image ResizeImage(Image srcImage, int newWidth, int newHeight) 
         {
             Bitmap newImage = new Bitmap(newWidth, newHeight);
             using (Graphics gr = Graphics.FromImage(newImage))
             {
                 gr.SmoothingMode = SmoothingMode.AntiAlias;
                 gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                 gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                 gr.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
             }
             return newImage;
         }
        static int SortSmile(SmileData sm1, SmileData sm2)
        {
            return sm1.Position - sm2.Position;
        }

        public static List<SmileData> GetSmilesOnMessage(string message, out string[] splited) 
        {
            List<SmileData> res = new List<SmileData>();
            
            foreach (string smile in smiles.Keys) 
            {
                
                int idx = message.IndexOf(smile);
                if (idx >= 0) 
                {
                    res.Add(new SmileData() { Smile = smiles[smile], Position = idx });
                }
            }
            res.Sort(new Comparison<SmileData>(SortSmile));
            splited = message.Split(smiles_text,StringSplitOptions.None);
            
            return res;
        } 
        //public static List<SmileData> ParseData(string message)
        //{
        //    List<SmileData> smiles = new List<SmileData>();
          
        //}

    }

    class SmileData
    {
        public Image Smile;
        public int Position;
    }
}
