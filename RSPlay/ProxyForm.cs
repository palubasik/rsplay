﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSPlay.Engine;
using System.Net;
using System.IO;

namespace RSPlay
{
    public partial class ProxyForm : Form
    {
        string webProxy;
        
        public ProxyForm()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = checkBox1.Checked;
        }

        private void ProxyForm_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = SettingInfo.Instance.SettingData.UseProxy;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            webProxy = string.Format("http://{0}:{1}", textBox1.Text, textBox2.Text);
            if (checkBox1.Checked) 
            {
                if (!TestProxy())
                {
                    MessageBox.Show("Проверка прокси закончилась неудачно, проверьте введенные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                SettingInfo.Instance.SettingData.WebProxy = webProxy;
                WebEngineBase.GlobalWebProxy = webProxy;
                SettingInfo.Instance.Save();
            }
            SettingInfo.Instance.SettingData.UseProxy = checkBox1.Checked;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private bool TestProxy()
        {
            bool res = false;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://google.com");
                request.Method = "GET";

                request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.64 Safari/532.5";
                request.AllowAutoRedirect = true;

                WebProxy proxy = new WebProxy();
                Uri uri = new Uri(webProxy);
                proxy.Address = uri;
                request.Proxy = proxy;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                reader.ReadToEnd();
                res = true;
            }
            catch { }
            return res;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webProxy = string.Format("http://{0}:{1}", textBox1.Text, textBox2.Text);
            if (!TestProxy())
            {
                MessageBox.Show("Проверка прокси закончилась неудачно, проверьте введенные данные", "Проверка прокси", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }else
                MessageBox.Show("ОК", "Проверка прокси", MessageBoxButtons.OK, MessageBoxIcon.Information);
       
        }
    }
}
