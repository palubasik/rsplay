﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Common
{
   public abstract class SingletonBased<T>  where T : class 
    {
          private static volatile T instance;
            private static object syncRoot = new Object();

            
            public static T Instance
            {
                get
                {

                    if (instance == null)
                    {
                        lock (syncRoot)
                        {
                            if (instance == null)
                                instance = typeof(T).InvokeMember(typeof(T).Name,
                                BindingFlags.CreateInstance |
                                BindingFlags.Instance |
                                BindingFlags.Public |
                                BindingFlags.NonPublic,
                                null, null, null) as T;
                        }
                    }
                    return instance;
                }
            }

            protected SingletonBased()
            {

            }
    }
}
