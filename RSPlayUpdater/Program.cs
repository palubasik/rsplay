﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using Common;
using System.Threading;
using System.Reflection;
using System.Windows.Forms;

namespace RSPlayUpdater
{
    class Program
    {
         const string UPDATE_FILE_PATH = "http://dl.dropbox.com/u/18364084/RSPlay.zip";
         const string UPDATE_FILE_UPDATER_PATH = "http://dl.dropbox.com/u/18364084/RSPlayUpdater.exe";
         static readonly string UPDATE_FILE_LOCAL_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RSPlay.zip");
         static readonly string UPDATE_FILE_UPDATER_LOCAL_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RSPlayUpdater.exe");
         static readonly string APP_LOCAL_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RSPlay.exe");

      
        static void Main(string[] args)
        {
            string updateLocalPath = UPDATE_FILE_LOCAL_PATH;
            string appLocalPath = APP_LOCAL_PATH;

            if (args.Length > 0) 
            {
                updateLocalPath = Path.Combine(args[0], "RSPlay.zip");
                appLocalPath = Path.Combine(args[0], "RSPlay.exe");
            }

            Console.WriteLine("RadioStart Player start installing/updating...");
            try
            {
                Thread.Sleep(1000);
                Console.Write("Downloading data...");
                WebClient wc = new WebClient();
                wc.DownloadFile(UPDATE_FILE_PATH, updateLocalPath);
                Console.WriteLine("OK!");
                Console.Write("Unpacking data...");
                Thread.Sleep(1000);
                ZipUtil.UnZipFiles(updateLocalPath, AppDomain.CurrentDomain.BaseDirectory, string.Empty, true);
                Console.WriteLine("OK!");
                try
                {
                    MailSender.Instance.Subject = string.Format("Update version on {0}", Environment.MachineName);
                    MailSender.Instance.Message = String.Format("{0}{1}{2}{1}", Environment.MachineName, Environment.NewLine, Environment.OSVersion.VersionString);
                    MailSender.Instance.SendMailLocal();
                }
                catch { }

                System.Diagnostics.Process.Start(appLocalPath);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error while update {0}", ex.Message);
            }
        }

    }
}
