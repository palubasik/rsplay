﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Xml;
using Common;

namespace Common
{
    public class MailSender : SingletonBased<MailSender>
    {
        string _smtp_server;
        string _mail_user;
        string _mail_password;
        string _mail_from;
        string _mail_to;
        string _subject;

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        int _port;
        bool _is_html;

        public bool Is_html
        {
            get { return _is_html; }
            set { _is_html = value; }
        }
        string[] messageAttachments;
        object locker = new object();

        private MailSender()
        {
            _smtp_server = "smtp.mail.ru";
            _port = 2525;
            _mail_user = "rsplay@mail.ru";
            _mail_from = "rsplay@mail.ru";
            _mail_password = "rsplay111";
            _mail_to = "palubasik@tut.by";
            _is_html = false;
        }

   
        public bool SendMailLocal(string mail_to, string subject, string message, bool is_html)
        {
            return SendMailLocal(_smtp_server, _mail_user, _mail_password,
            _mail_from, mail_to, subject, message, is_html);
        }

        public bool SendMailLocal(string mail_to, string subject, string message)
        {
            return SendMailLocal(_smtp_server, _mail_user, _mail_password,
            _mail_from, mail_to, subject, message, _is_html);
        }

        public bool SendMailLocal(string subject, string message)
        {
            return SendMailLocal(_smtp_server, _mail_user, _mail_password,
            _mail_from, _mail_from, subject, message, _is_html);
        }

        public bool SendMailLocal()
        {
            return SendMailLocal(_smtp_server, _mail_user, _mail_password,
            _mail_from, _mail_to, _subject, _message, _is_html);
        }

        public bool SendMailLocal(string smtp_server, string mail_user, string mail_password,
            string mail_from, string mail_to, string subject, string message, bool is_html)
        {
            if (string.IsNullOrEmpty(mail_to) || mail_to.Contains("@") == false)
                return false;

            lock (locker)
            {
                System.Net.Mail.MailMessage mess = new System.Net.Mail.MailMessage( );
                mess.From = new MailAddress(mail_from);
                foreach (string ss in mail_to.Split(';'))
                    mess.To.Add(ss);
                mess.Subject = subject; 
                mess.Body = message;
                mess.SubjectEncoding = System.Text.Encoding.UTF8;
                mess.BodyEncoding = System.Text.Encoding.UTF8;

                mess.IsBodyHtml = is_html;
                if(messageAttachments != null)
                foreach (string atch in messageAttachments)
                {
                    mess.Attachments.Add(new Attachment(atch));
                }

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtp_server, _port);

                if (string.IsNullOrEmpty(mail_user))
                {
                    client.UseDefaultCredentials = true;
                }
                else
                {
                    client.Credentials = new System.Net.NetworkCredential(mail_user, mail_password);
                }
                
                client.Send(mess);
            }

            return true;
        }

        public void AddAttachments(string[] filenames) 
        {
            messageAttachments = filenames;
        }

        protected void ClearAttachments() 
        {
            messageAttachments = null;
        }
    }


}
