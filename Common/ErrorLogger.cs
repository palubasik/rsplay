﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Diagnostics;

namespace Common
{
    public delegate void ErrorHandler(Exception e, string Message);

    [Flags]
    public enum LogDirection
    {
        NONE = 0x0,
        FILE = 0x1,
        DATABASE = 0x2,
        EVENTLOG = 0x4,
        SERVICE = 0x8
    }

    public class ErrorLogger : SingletonBased<ErrorLogger>
    {
        string base_path = AppDomain.CurrentDomain.BaseDirectory;


        string logFile;
        LogDirection logDirection;
        int maxLogFileSize;
        string eventLogSource;
        string eventLogName;
        string connectionString;

        object lockIO = new object();

        public event ErrorHandler CustomError;
        public string LogFile
        {
            get { return logFile; }
        }

        public string EventLogSource
        {
            get { return eventLogSource; }
        }

        public string EventLogName
        {
            get { return eventLogName; }
        }

        public int MaxLogFileSize
        {
            get { return maxLogFileSize; }
        }

        public LogDirection LogDirection
        {
            get { return logDirection; }
        }

        public string LogPath
        {
            get { return base_path; }
            set { base_path = value; }
        }

        public ErrorLogger()
        {
            logFile = ConfigurationManager.AppSettings["LogFile"] != null ? ConfigurationManager.AppSettings["LogFile"] :"error_log.txt";

            base_path = ConfigurationManager.AppSettings["LogPath"] != null ? ConfigurationManager.AppSettings["LogPath"] : AppDomain.CurrentDomain.BaseDirectory;

            logFile = Path.Combine(base_path, logFile);

            if (ConfigurationManager.AppSettings["LogDirection"] != null)
            {
                try
                {
                    logDirection = (LogDirection)Enum.Parse(logDirection.GetType(), ConfigurationManager.AppSettings["LogDirection"]);
                }
                catch
                {
                    logDirection = LogDirection.FILE;
                }
            }
            else
                logDirection = LogDirection.FILE;

            if (ConfigurationManager.AppSettings["MaxFileLog"] == null || !int.TryParse(ConfigurationManager.AppSettings["MaxFileLog"], out maxLogFileSize))
                maxLogFileSize = 70000;

            eventLogSource = ConfigurationManager.AppSettings["EventLogSource"] != null ? ConfigurationManager.AppSettings["EventLogSource"] : AppDomain.CurrentDomain.FriendlyName;
            eventLogName = ConfigurationManager.AppSettings["EventLogName"] != null ? ConfigurationManager.AppSettings["EventLogName"] : "Error";

            //if (logDirection.IsSet(LogDirection.EVENTLOG))
            //{
            //    if (!EventLog.SourceExists(eventLogSource))
            //    {
            //        EventLog.CreateEventSource(eventLogSource, eventLogName);
            //    }
            //}
            if (ConfigurationManager.AppSettings["LogConnectionString"] != null)
                connectionString = ConfigurationManager.ConnectionStrings["LogConnectionString"].ToString();
        }

        object LoadConfigValue(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }
        void ThrowCustomError(Exception e, string message)
        {
            if (CustomError != null)
                CustomError(e, message);
        }

        public void WriteError(Exception e)
        {
            WriteError(e, e.Message);
        }

        public void WriteError(Exception e, string message)
        {
            try
            {

                //if (logDirection.IsSet(LogDirection.EVENTLOG))
                //    WriteErrorEventLog(e, message);

                
                WriteErrorFile(e, message);

                //if (logDirection.IsSet(LogDirection.SERVICE))
                //    WriteErrorService(e, message);

            }
            catch (Exception ex)
            {
                WriteErrorFile(ex, String.Format("Error write exception to {0}", logDirection));
                WriteErrorFile(e, message);
            }
            finally
            {
                ThrowCustomError(e, e.Message);
            }
        }

        private void WriteErrorService(Exception e, string message)
        {
            throw new NotImplementedException();
        }

        private void WriteErrorDB(Exception e, string message)
        {
            throw new NotImplementedException();
            //LogError lg = new LogError();
            //lg.MachineName = Environment.MachineName;
            //lg.SetAutoTruncate("Message", message);
            //lg.SetAutoTruncate("StackTrace", e.StackTrace);
            //if (e.TargetSite != null && e.TargetSite.Name != null)
            //    lg.SetAutoTruncate("MethodName", e.TargetSite.Name);
            //lg.SetAutoTruncate("Source", e.Source);

            //lg.CreateDate = DateTime.Now;

            //using (PetDataClassesDataContext db = connectionString == null ? new PetDataClassesDataContext() : new PetDataClassesDataContext(connectionString))
            //{
            //    db.LogErrors.InsertOnSubmit(lg);
            //    db.SubmitOverwrite();
            //}
        }

        private void WriteErrorFile(Exception e, string message)
        {
            string error_message = FormatErrorMessage(e, message);

            lock (lockIO)
            {
                if (File.Exists(logFile) && (new FileInfo(logFile)).Length > maxLogFileSize)
                    File.Delete(logFile);

                File.AppendAllText(logFile, error_message,Encoding.UTF8);
            }
        }

        private string FormatErrorMessage(Exception e, string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("=======================================\n");
            sb.AppendFormat("Date       : {0}\n", DateTime.Now.ToLongTimeString());
            sb.AppendFormat("Time       : {0}\n", DateTime.Now.ToShortDateString());
            if (e != null)
            {
                sb.AppendFormat("Method     : {0}\n", e.TargetSite != null ? e.TargetSite.Name != null ? e.TargetSite.Name.ToString() : "null" : "null");
                sb.AppendFormat("Stack Trace	: {0}\n", (e.StackTrace != null ? e.StackTrace.ToString().Trim() : "null"));
                sb.AppendFormat("Source		: {0}\n", (e.Source != null ? e.Source.ToString().Trim() : "null"));
                sb.AppendFormat("Error		: {0}\n", (e.Message != null ? e.Message.ToString().Trim() : "null"));

            }
            else
                sb.Append("Exception is null\n");
            if (!String.IsNullOrEmpty(message))
                sb.AppendFormat("Message		: {0}\n", message);
            return sb.ToString();
        }

        private void WriteErrorEventLog(Exception e, string message)
        {
            EventLog aLog = new EventLog();
            aLog.Source = eventLogSource;
            aLog.WriteEntry(FormatErrorMessage(e, message), EventLogEntryType.Error);
        }
    }


}
