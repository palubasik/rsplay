﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Web;
using System.Reflection;
using System.Collections;

namespace Common
{
    public static class Helper
    {
        private static Hashtable javaScriptSymbolDictionary = new Hashtable();
        static Helper() 
        {
            //        function encodeMyHtml() {
            //  encodedHtml = escape(encodeHtml.htmlToEncode.value);
            //  encodedHtml = encodedHtml.replace(/\//g,"%2F");
            //  encodedHtml = encodedHtml.replace(/\?/g,"%3F");
            //  encodedHtml = encodedHtml.replace(/=/g,"%3D");
            //  encodedHtml = encodedHtml.replace(/&/g,"%26");
            //  encodedHtml = encodedHtml.replace(/@/g,"%40");
            //  encodeHtml.htmlEncoded.value = encodedHtml;
            //} 
            using (Stream stream = Assembly.GetExecutingAssembly()
                               .GetManifestResourceStream("Common.urlencode.txt"))
            using (StreamReader reader = new StreamReader(stream))
            {
                while (reader.Peek() > -1) 
                {
                    string line = reader.ReadLine();
                    string[] splitted = line.Split('\t');
                    javaScriptSymbolDictionary[splitted[0]] = splitted[1];
                }
            }
        }
        public static string HTMLURLEncode(string data) 
        {
            return HTMLURLEncode(data, true);
        }

        public static string HTMLURLDecode(string data)
        {
            return HTMLURLEncode(data, false);
        }

        private static string HTMLURLEncode(string data,bool encode)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0, len = data.Length; i < len; i++)
            {
                string appendItem = data[i].ToString();
                if (encode)
                {
                    if (javaScriptSymbolDictionary.ContainsKey(appendItem))
                        appendItem = javaScriptSymbolDictionary[appendItem].ToString();
                }
                else
                {
                    foreach (DictionaryEntry de in javaScriptSymbolDictionary)
                    {
                        if (de.Value.ToString() == appendItem)
                        {
                            appendItem = de.Value.ToString();
                            break;
                        }
                    }
                }
                sb.Append(appendItem);
            }
            return sb.ToString();
        }



        public static bool ExitProgram = false;
        public static void UIThread(Form form, MethodInvoker code)
        {
            if (ExitProgram)
                return;
            if (form.InvokeRequired)
            {
                form.Invoke(code);
                return;
            }
            code.Invoke();
        }


       
        public static XmlDocument GetXMLFromHtml(string Msg)
        {

            // setup SgmlReader
            Sgml.SgmlReader sgmlReader = new Sgml.SgmlReader();
            sgmlReader.DocType = "HTML";
            sgmlReader.WhitespaceHandling = WhitespaceHandling.All;
            sgmlReader.CaseFolding = Sgml.CaseFolding.ToLower;
            sgmlReader.InputStream = new StringReader(Msg);

            // create document
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.XmlResolver = null;
            doc.Load(sgmlReader);
            return doc;
        }

        public static void OpenLink(string sUrl)
        {
            try
            {
                if (!sUrl.Contains("http://") && !sUrl.Contains("https://"))
                    sUrl = "http://" + sUrl;
                System.Diagnostics.Process.Start(sUrl);
            }
            catch (Exception exc1)
            {
                if (exc1.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {
                    try
                    {
                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("IExplore.exe", sUrl);
                        System.Diagnostics.Process.Start(startInfo);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Невозможно открыть браузер!", "RadioStart player", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

    }

}
