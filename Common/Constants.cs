﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Common
{
    public static class Constants
    {
        public const string APP_NAME = "RadioSTART Player";
        public const string SETTING_FILE = "data.bin";
        public const string Version = "1.9";
        public const string UPDATE_URL = "http://dl.dropbox.com/u/18364084/radiostart/RSPlay.html";
        public const string FOLLOW_ME = "https://twitter.com/#!/palubasik";
        public static List<string> FileList = new List<string>();
        public const string UPDATE_FILE_PATH = "http://dl.dropbox.com/u/18364084/RSPlay.zip";
        public const string UPDATE_FILE_UPDATER_PATH = "http://dl.dropbox.com/u/18364084/RSPlayUpdater.exe";
        public static readonly string UPDATE_FILE_LOCAL_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RSPlay.zip");
        public static readonly string UPDATE_FILE_UPDATER_LOCAL_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RSPlayUpdater.exe");
        public static readonly string APP_LOCAL_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RSPlay.exe");
        public const int START_VOLUME = 70;
        public static readonly string CHAT_HISTORY_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ChatHistory.log");
      
        public static class Twitter 
        {
            public const string CONSUMER_KEY = "bChZ1x5bl07z46FLN6FsAA";
            public const string CONSUMER_SECRET = "UBXzQrs4BiZjJ5Nr41bF2L2Ag3FCJXU65VwL6z14";
        }

        public static class LastFM 
        {
            public const string CONSUMER_KEY = "0343a5786180214eece29534287e9b7b";
            public const string CONSUMER_SECRET = "a80869a34e0dda95308ebe8555ea3c3f";
        }
        public static class RadioStartSite
        {
            public const string SERVER_STREAM_URL = "http://93.84.112.253:8002/radiostart";
            public const string SERVER_URL = "http://radiostart.by";
            public const string SERVER_URL_RSS = "http://radiostart.by/rss";
            public const string SERVER_CHAT_URL = "http://www.radiostart.by/wp-content/phpfreechat-1.3/";
           
        }
        static Constants()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            FileList.Add(Path.Combine(path, SETTING_FILE));
            FileList.Add(Application.ExecutablePath);
        }

        public const string HTML_TEMPLATE = "<html><head></head><body>{0}</body></html>";
    }

}
